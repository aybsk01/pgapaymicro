package ru.iconsoft.pgapaymicro.repository;
import ru.iconsoft.pgapaymicro.domain.Payment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Payment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query("select p from Payment p where p.paymentcard.id = ?1")
    Payment findByCardId(Long cardIdentity);
}
