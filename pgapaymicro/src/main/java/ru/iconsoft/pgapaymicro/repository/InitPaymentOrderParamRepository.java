package ru.iconsoft.pgapaymicro.repository;
import ru.iconsoft.pgapaymicro.domain.InitPaymentOrderParam;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the InitPaymentOrderParam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InitPaymentOrderParamRepository extends JpaRepository<InitPaymentOrderParam, Long> {

}
