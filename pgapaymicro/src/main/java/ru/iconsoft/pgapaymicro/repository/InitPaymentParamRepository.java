package ru.iconsoft.pgapaymicro.repository;
import ru.iconsoft.pgapaymicro.domain.InitPaymentParam;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the InitPaymentParam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InitPaymentParamRepository extends JpaRepository<InitPaymentParam, Long> {
    @Query("select ipp from InitPaymentParam ipp where ipp.orderId = ?1")
    InitPaymentParam findByOrderId(String orderId);
    @Query("select ipp from InitPaymentParam ipp where ipp.payment.id = ?1")
    InitPaymentParam findByPaymentId(Long payId);
}
