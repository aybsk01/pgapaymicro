package ru.iconsoft.pgapaymicro.repository;
import ru.iconsoft.pgapaymicro.domain.PaymentCard;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PaymentCard entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentCardRepository extends JpaRepository<PaymentCard, Long> {
    @Query("select pc from PaymentCard pc where pc.userId = ?1")
    PaymentCard findByUserId(String userId);
}
