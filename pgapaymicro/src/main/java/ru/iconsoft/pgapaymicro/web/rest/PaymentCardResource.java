package ru.iconsoft.pgapaymicro.web.rest;

import ru.iconsoft.pgapaymicro.domain.PaymentCard;
import ru.iconsoft.pgapaymicro.service.PaymentCardService;
import ru.iconsoft.pgapaymicro.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.iconsoft.pgapaymicro.domain.PaymentCard}.
 */
@RestController
@RequestMapping("/api")
public class PaymentCardResource {

    private final Logger log = LoggerFactory.getLogger(PaymentCardResource.class);

    private static final String ENTITY_NAME = "pgapaymicroPaymentCard";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentCardService paymentCardService;

    public PaymentCardResource(PaymentCardService paymentCardService) {
        this.paymentCardService = paymentCardService;
    }

    /**
     * {@code POST  /payment-cards} : Create a new paymentCard.
     *
     * @param paymentCard the paymentCard to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paymentCard, or with status {@code 400 (Bad Request)} if the paymentCard has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/payment-cards")
    public ResponseEntity<PaymentCard> createPaymentCard(@Valid @RequestBody PaymentCard paymentCard) throws URISyntaxException {
        log.debug("REST request to save PaymentCard : {}", paymentCard);
        if (paymentCard.getId() != null) {
            throw new BadRequestAlertException("A new paymentCard cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentCard result = paymentCardService.save(paymentCard);
        return ResponseEntity.created(new URI("/api/payment-cards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /payment-cards} : Updates an existing paymentCard.
     *
     * @param paymentCard the paymentCard to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentCard,
     * or with status {@code 400 (Bad Request)} if the paymentCard is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paymentCard couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/payment-cards")
    public ResponseEntity<PaymentCard> updatePaymentCard(@Valid @RequestBody PaymentCard paymentCard) throws URISyntaxException {
        log.debug("REST request to update PaymentCard : {}", paymentCard);
        if (paymentCard.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PaymentCard result = paymentCardService.save(paymentCard);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, paymentCard.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /payment-cards} : get all the paymentCards.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paymentCards in body.
     */
    @GetMapping("/payment-cards")
    public List<PaymentCard> getAllPaymentCards() {
        log.debug("REST request to get all PaymentCards");
        return paymentCardService.findAll();
    }

    /**
     * {@code GET  /payment-cards/:id} : get the "id" paymentCard.
     *
     * @param id the id of the paymentCard to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paymentCard, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/payment-cards/{id}")
    public ResponseEntity<PaymentCard> getPaymentCard(@PathVariable Long id) {
        log.debug("REST request to get PaymentCard : {}", id);
        Optional<PaymentCard> paymentCard = paymentCardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(paymentCard);
    }

    /**
     * {@code DELETE  /payment-cards/:id} : delete the "id" paymentCard.
     *
     * @param id the id of the paymentCard to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/payment-cards/{id}")
    public ResponseEntity<Void> deletePaymentCard(@PathVariable Long id) {
        log.debug("REST request to delete PaymentCard : {}", id);
        paymentCardService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
