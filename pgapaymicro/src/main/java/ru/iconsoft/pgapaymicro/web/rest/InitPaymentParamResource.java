package ru.iconsoft.pgapaymicro.web.rest;

import ru.iconsoft.pgapaymicro.domain.InitPaymentParam;
import ru.iconsoft.pgapaymicro.service.InitPaymentParamService;
import ru.iconsoft.pgapaymicro.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.iconsoft.pgapaymicro.domain.InitPaymentParam}.
 */
@RestController
@RequestMapping("/api")
public class InitPaymentParamResource {

    private final Logger log = LoggerFactory.getLogger(InitPaymentParamResource.class);

    private static final String ENTITY_NAME = "pgapaymicroInitPaymentParam";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InitPaymentParamService initPaymentParamService;

    public InitPaymentParamResource(InitPaymentParamService initPaymentParamService) {
        this.initPaymentParamService = initPaymentParamService;
    }

    /**
     * {@code POST  /init-payment-params} : Create a new initPaymentParam.
     *
     * @param initPaymentParam the initPaymentParam to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new initPaymentParam, or with status {@code 400 (Bad Request)} if the initPaymentParam has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/init-payment-params")
    public ResponseEntity<InitPaymentParam> createInitPaymentParam(@Valid @RequestBody InitPaymentParam initPaymentParam) throws URISyntaxException {
        log.debug("REST request to save InitPaymentParam : {}", initPaymentParam);
        if (initPaymentParam.getId() != null) {
            throw new BadRequestAlertException("A new initPaymentParam cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InitPaymentParam result = initPaymentParamService.save(initPaymentParam);
        return ResponseEntity.created(new URI("/api/init-payment-params/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /init-payment-params} : Updates an existing initPaymentParam.
     *
     * @param initPaymentParam the initPaymentParam to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated initPaymentParam,
     * or with status {@code 400 (Bad Request)} if the initPaymentParam is not valid,
     * or with status {@code 500 (Internal Server Error)} if the initPaymentParam couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/init-payment-params")
    public ResponseEntity<InitPaymentParam> updateInitPaymentParam(@Valid @RequestBody InitPaymentParam initPaymentParam) throws URISyntaxException {
        log.debug("REST request to update InitPaymentParam : {}", initPaymentParam);
        if (initPaymentParam.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InitPaymentParam result = initPaymentParamService.save(initPaymentParam);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, initPaymentParam.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /init-payment-params} : get all the initPaymentParams.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of initPaymentParams in body.
     */
    @GetMapping("/init-payment-params")
    public List<InitPaymentParam> getAllInitPaymentParams() {
        log.debug("REST request to get all InitPaymentParams");
        return initPaymentParamService.findAll();
    }

    /**
     * {@code GET  /init-payment-params/:id} : get the "id" initPaymentParam.
     *
     * @param id the id of the initPaymentParam to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the initPaymentParam, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/init-payment-params/{id}")
    public ResponseEntity<InitPaymentParam> getInitPaymentParam(@PathVariable Long id) {
        log.debug("REST request to get InitPaymentParam : {}", id);
        Optional<InitPaymentParam> initPaymentParam = initPaymentParamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(initPaymentParam);
    }

    /**
     * {@code DELETE  /init-payment-params/:id} : delete the "id" initPaymentParam.
     *
     * @param id the id of the initPaymentParam to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/init-payment-params/{id}")
    public ResponseEntity<Void> deleteInitPaymentParam(@PathVariable Long id) {
        log.debug("REST request to delete InitPaymentParam : {}", id);
        initPaymentParamService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
