/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.iconsoft.pgapaymicro.web.rest.vm;
