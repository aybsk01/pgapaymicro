package ru.iconsoft.pgapaymicro.web.rest;

import ru.iconsoft.pgapaymicro.domain.InitPaymentOrderParam;
import ru.iconsoft.pgapaymicro.service.InitPaymentOrderParamService;
import ru.iconsoft.pgapaymicro.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.iconsoft.pgapaymicro.domain.InitPaymentOrderParam}.
 */
@RestController
@RequestMapping("/api")
public class InitPaymentOrderParamResource {

    private final Logger log = LoggerFactory.getLogger(InitPaymentOrderParamResource.class);

    private static final String ENTITY_NAME = "pgapaymicroInitPaymentOrderParam";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InitPaymentOrderParamService initPaymentOrderParamService;

    public InitPaymentOrderParamResource(InitPaymentOrderParamService initPaymentOrderParamService) {
        this.initPaymentOrderParamService = initPaymentOrderParamService;
    }

    /**
     * {@code POST  /init-payment-order-params} : Create a new initPaymentOrderParam.
     *
     * @param initPaymentOrderParam the initPaymentOrderParam to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new initPaymentOrderParam, or with status {@code 400 (Bad Request)} if the initPaymentOrderParam has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/init-payment-order-params")
    public ResponseEntity<InitPaymentOrderParam> createInitPaymentOrderParam(@Valid @RequestBody InitPaymentOrderParam initPaymentOrderParam) throws URISyntaxException {
        log.debug("REST request to save InitPaymentOrderParam : {}", initPaymentOrderParam);
        if (initPaymentOrderParam.getId() != null) {
            throw new BadRequestAlertException("A new initPaymentOrderParam cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InitPaymentOrderParam result = initPaymentOrderParamService.save(initPaymentOrderParam);
        return ResponseEntity.created(new URI("/api/init-payment-order-params/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /init-payment-order-params} : Updates an existing initPaymentOrderParam.
     *
     * @param initPaymentOrderParam the initPaymentOrderParam to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated initPaymentOrderParam,
     * or with status {@code 400 (Bad Request)} if the initPaymentOrderParam is not valid,
     * or with status {@code 500 (Internal Server Error)} if the initPaymentOrderParam couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/init-payment-order-params")
    public ResponseEntity<InitPaymentOrderParam> updateInitPaymentOrderParam(@Valid @RequestBody InitPaymentOrderParam initPaymentOrderParam) throws URISyntaxException {
        log.debug("REST request to update InitPaymentOrderParam : {}", initPaymentOrderParam);
        if (initPaymentOrderParam.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InitPaymentOrderParam result = initPaymentOrderParamService.save(initPaymentOrderParam);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, initPaymentOrderParam.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /init-payment-order-params} : get all the initPaymentOrderParams.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of initPaymentOrderParams in body.
     */
    @GetMapping("/init-payment-order-params")
    public List<InitPaymentOrderParam> getAllInitPaymentOrderParams() {
        log.debug("REST request to get all InitPaymentOrderParams");
        return initPaymentOrderParamService.findAll();
    }

    /**
     * {@code GET  /init-payment-order-params/:id} : get the "id" initPaymentOrderParam.
     *
     * @param id the id of the initPaymentOrderParam to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the initPaymentOrderParam, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/init-payment-order-params/{id}")
    public ResponseEntity<InitPaymentOrderParam> getInitPaymentOrderParam(@PathVariable Long id) {
        log.debug("REST request to get InitPaymentOrderParam : {}", id);
        Optional<InitPaymentOrderParam> initPaymentOrderParam = initPaymentOrderParamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(initPaymentOrderParam);
    }

    /**
     * {@code DELETE  /init-payment-order-params/:id} : delete the "id" initPaymentOrderParam.
     *
     * @param id the id of the initPaymentOrderParam to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/init-payment-order-params/{id}")
    public ResponseEntity<Void> deleteInitPaymentOrderParam(@PathVariable Long id) {
        log.debug("REST request to delete InitPaymentOrderParam : {}", id);
        initPaymentOrderParamService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
