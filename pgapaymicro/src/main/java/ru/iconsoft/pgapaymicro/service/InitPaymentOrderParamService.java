package ru.iconsoft.pgapaymicro.service;

import ru.iconsoft.pgapaymicro.domain.InitPaymentOrderParam;
import ru.iconsoft.pgapaymicro.repository.InitPaymentOrderParamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link InitPaymentOrderParam}.
 */
@Service
@Transactional
public class InitPaymentOrderParamService {

    private final Logger log = LoggerFactory.getLogger(InitPaymentOrderParamService.class);

    private final InitPaymentOrderParamRepository initPaymentOrderParamRepository;

    public InitPaymentOrderParamService(InitPaymentOrderParamRepository initPaymentOrderParamRepository) {
        this.initPaymentOrderParamRepository = initPaymentOrderParamRepository;
    }

    /**
     * Save a initPaymentOrderParam.
     *
     * @param initPaymentOrderParam the entity to save.
     * @return the persisted entity.
     */
    public InitPaymentOrderParam save(InitPaymentOrderParam initPaymentOrderParam) {
        log.debug("Request to save InitPaymentOrderParam : {}", initPaymentOrderParam);
        return initPaymentOrderParamRepository.save(initPaymentOrderParam);
    }

    /**
     * Get all the initPaymentOrderParams.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<InitPaymentOrderParam> findAll() {
        log.debug("Request to get all InitPaymentOrderParams");
        return initPaymentOrderParamRepository.findAll();
    }


    /**
     * Get one initPaymentOrderParam by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<InitPaymentOrderParam> findOne(Long id) {
        log.debug("Request to get InitPaymentOrderParam : {}", id);
        return initPaymentOrderParamRepository.findById(id);
    }

    /**
     * Delete the initPaymentOrderParam by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete InitPaymentOrderParam : {}", id);
        initPaymentOrderParamRepository.deleteById(id);
    }
}
