package ru.iconsoft.pgapaymicro.service;

import ru.iconsoft.pgapaymicro.domain.PaymentCard;
import ru.iconsoft.pgapaymicro.repository.PaymentCardRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link PaymentCard}.
 */
@Service
@Transactional
public class PaymentCardService {

    private final Logger log = LoggerFactory.getLogger(PaymentCardService.class);

    private final PaymentCardRepository paymentCardRepository;

    public PaymentCardService(PaymentCardRepository paymentCardRepository) {
        this.paymentCardRepository = paymentCardRepository;
    }

    /**
     * Save a paymentCard.
     *
     * @param paymentCard the entity to save.
     * @return the persisted entity.
     */
    public PaymentCard save(PaymentCard paymentCard) {
        log.debug("Request to save PaymentCard : {}", paymentCard);
        return paymentCardRepository.save(paymentCard);
    }

    /**
     * Get all the paymentCards.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PaymentCard> findAll() {
        log.debug("Request to get all PaymentCards");
        return paymentCardRepository.findAll();
    }


    /**
     * Get one paymentCard by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PaymentCard> findOne(Long id) {
        log.debug("Request to get PaymentCard : {}", id);
        return paymentCardRepository.findById(id);
    }

    /**
     * Delete the paymentCard by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentCard : {}", id);
        paymentCardRepository.deleteById(id);
    }
}
