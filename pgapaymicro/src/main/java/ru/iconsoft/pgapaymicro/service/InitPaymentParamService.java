package ru.iconsoft.pgapaymicro.service;

import ru.iconsoft.pgapaymicro.domain.InitPaymentParam;
import ru.iconsoft.pgapaymicro.repository.InitPaymentParamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link InitPaymentParam}.
 */
@Service
@Transactional
public class InitPaymentParamService {

    private final Logger log = LoggerFactory.getLogger(InitPaymentParamService.class);

    private final InitPaymentParamRepository initPaymentParamRepository;

    public InitPaymentParamService(InitPaymentParamRepository initPaymentParamRepository) {
        this.initPaymentParamRepository = initPaymentParamRepository;
    }

    /**
     * Save a initPaymentParam.
     *
     * @param initPaymentParam the entity to save.
     * @return the persisted entity.
     */
    public InitPaymentParam save(InitPaymentParam initPaymentParam) {
        log.debug("Request to save InitPaymentParam : {}", initPaymentParam);
        return initPaymentParamRepository.save(initPaymentParam);
    }

    /**
     * Get all the initPaymentParams.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<InitPaymentParam> findAll() {
        log.debug("Request to get all InitPaymentParams");
        return initPaymentParamRepository.findAll();
    }


    /**
     * Get one initPaymentParam by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<InitPaymentParam> findOne(Long id) {
        log.debug("Request to get InitPaymentParam : {}", id);
        return initPaymentParamRepository.findById(id);
    }

    /**
     * Delete the initPaymentParam by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete InitPaymentParam : {}", id);
        initPaymentParamRepository.deleteById(id);
    }
}
