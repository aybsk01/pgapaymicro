package ru.iconsoft.pgapaymicro.service.pga;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iconsoft.pgapaymicro.domain.InitPaymentParam;
import ru.iconsoft.pgapaymicro.domain.Payment;
import ru.iconsoft.pgapaymicro.domain.PaymentCard;
import ru.iconsoft.pgapaymicro.domain.enumeration.PaymentStatus;
import ru.iconsoft.pgapaymicro.repository.InitPaymentParamRepository;
import ru.iconsoft.pgapaymicro.repository.PaymentCardRepository;
import ru.iconsoft.pgapaymicro.repository.PaymentRepository;
import ru.iconsoft.pgapaymicro.service.pga.dto.*;

@Service
@Transactional
public class PgaService {
    private PaymentRepository paymentRepository;
    private PaymentCardRepository paymentCardRepository;
    private InitPaymentParamRepository initPaymentParamRepository;

    public PgaService(PaymentRepository paymentRepository,
                      PaymentCardRepository paymentCardRepository,
                      InitPaymentParamRepository initPaymentParamRepository){
        this.paymentRepository = paymentRepository;
        this.paymentCardRepository = paymentCardRepository;
        this.initPaymentParamRepository = initPaymentParamRepository;
    }

    public CpaResponseDto startPaymentTransaction(CpaRequestDto payParams){
        if ((payParams == null) || !payParams.isValid())
            return new CpaResponseDto(2, "Error incoming request");

        if ((payParams.getO() == null) || (payParams.getO().getOrder_id() == null)){
            return new CpaResponseDto(2, "Error. Order id not specified");
        }

        InitPaymentParam initPaymentParam = initPaymentParamRepository.findByOrderId(payParams.getO().getOrder_id());
        if (initPaymentParam == null){
            return new CpaResponseDto(2, "Error. Order not found");
        }

        Payment payment = new Payment();
        payment.setPgaTrxId(payParams.getTrx_id());
        payment.setStatus(PaymentStatus.NEW);
        payment.setTransactionDt(payParams.getTs().toInstant());
        //payment.setInitPaymentParam(initPaymentParam);
        initPaymentParam.setPayment(payment);
        paymentRepository.save(payment);
        initPaymentParamRepository.save(initPaymentParam);
        Long payId = payment.getId();

        CpaResponceCard customerCard = null;
        PaymentCard paymentCard = paymentCardRepository.findByUserId(initPaymentParam.getUserId());
        if (paymentCard != null){
            Payment prevPayment = paymentRepository.findByCardId(paymentCard.getId());
            if (prevPayment != null){
                customerCard = new CpaResponceCard();
                customerCard.setTrxId(prevPayment.getPgaTrxId());
                customerCard.setId(paymentCard.getCardId());
            }
        }

        //Тут нужно выяснить что нужно будет показывать на платежной странице
        CpaResponseDto responseDto = new CpaResponseDto(
            payId.toString(),
            new PgaResponseResult(1, "Payment available"),
            new CpaResponsePurchase(
                initPaymentParam.getPurchaseShortDesc(),
                initPaymentParam.getPurchaseLongDesc(),
                new CpaResponsePurchaseAccount(initPaymentParam.getAccountId(), initPaymentParam.getAmmount().longValue()))
        );
        if (customerCard != null){
            responseDto.setCard(customerCard);
        }

        return responseDto;
    }

    public RegisterPaymentResponse completePaymentTransaction(RegisterPaymentRequest rpReq, String fullUrl){

        if (!PgaSignUtil.checkUrlSign(fullUrl)){
            return new RegisterPaymentResponse(2, "Signature failed");
        }

        if (rpReq == null)
            return new RegisterPaymentResponse(2, "Error incoming request");

        Payment payment = paymentRepository.findById(Long.parseLong(rpReq.getMerchant_trx())).get();
        if (payment == null){
            return new RegisterPaymentResponse(2, "Error. Not founr merchant transaction");
        }

        payment.setStatus(rpReq.getResult_code().equals(1) ? PaymentStatus.COMPLETE : PaymentStatus.ERROR);
        payment.setExtResultCode(rpReq.getExt_result_code());
        //TO DO или сделать поле для фактической сумму или сравнить с исходной и выругатсья
        //if (payment.getInitPaymentParam().ammount())
        //payment.setPaymentAmount(rpReq.getAmount());
        payment.setTransactionDt(rpReq.getTs().toInstant());

        RegisterPaymentParams paymentParams = rpReq.getP();
        //To DO сделать отдельное поле для это даты времени в Payment
        //payment.setDt(paymentParams.getTransmissionDateTime().toInstant());
        payment.setRrn(paymentParams.getRrn());
        payment.setCardHolder(paymentParams.getCardHolder());
        payment.setAuthCode(paymentParams.getAuthcode());
        payment.setPan(paymentParams.getMaskedPan());
        payment.setFullAuth(paymentParams.getIsFullyAuthenticated().equals("Y"));

        RegisterPaymentCardParams cardParams = rpReq.getCard();
        if ((cardParams != null) && (cardParams.isValid()) && (cardParams.getRegistered().equals("Y"))){
            PaymentCard card = new PaymentCard();
            InitPaymentParam initPaymentParam = initPaymentParamRepository.findByPaymentId(payment.getId());
            card.setUserId(initPaymentParam.getUserId());
            card.setCardId(cardParams.getId());
            card.setExpireDt(cardParams.getExpiry());
            if ((cardParams.getRecurrent() != null) && (cardParams.getRecurrent().equals("Y"))){
                card.setRequrrent(true);
            }
            payment.setPaymentcard(card);
            paymentCardRepository.save(card);
        }
        paymentRepository.save(payment);
        return new RegisterPaymentResponse(1, "OK");
    }
}
