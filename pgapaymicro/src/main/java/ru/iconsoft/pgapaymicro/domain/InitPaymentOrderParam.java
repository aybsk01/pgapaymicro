package ru.iconsoft.pgapaymicro.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A InitPaymentOrderParam.
 */
@Entity
@Table(name = "init_payment_order_param")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InitPaymentOrderParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 32)
    @Column(name = "param_name", length = 32)
    private String paramName;

    @Size(max = 128)
    @Column(name = "param_value", length = 128)
    private String paramValue;

    @ManyToOne
    @JsonIgnoreProperties("initPaymentOrderParams")
    private InitPaymentParam initPaymentParam;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParamName() {
        return paramName;
    }

    public InitPaymentOrderParam paramName(String paramName) {
        this.paramName = paramName;
        return this;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public InitPaymentOrderParam paramValue(String paramValue) {
        this.paramValue = paramValue;
        return this;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public InitPaymentParam getInitPaymentParam() {
        return initPaymentParam;
    }

    public InitPaymentOrderParam initPaymentParam(InitPaymentParam initPaymentParam) {
        this.initPaymentParam = initPaymentParam;
        return this;
    }

    public void setInitPaymentParam(InitPaymentParam initPaymentParam) {
        this.initPaymentParam = initPaymentParam;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InitPaymentOrderParam)) {
            return false;
        }
        return id != null && id.equals(((InitPaymentOrderParam) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "InitPaymentOrderParam{" +
            "id=" + getId() +
            ", paramName='" + getParamName() + "'" +
            ", paramValue='" + getParamValue() + "'" +
            "}";
    }
}
