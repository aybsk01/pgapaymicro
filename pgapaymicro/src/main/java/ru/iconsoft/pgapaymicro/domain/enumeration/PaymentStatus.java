package ru.iconsoft.pgapaymicro.domain.enumeration;

/**
 * The PaymentStatus enumeration.
 */
public enum PaymentStatus {
    NEW, COMPLETE, ERROR
}
