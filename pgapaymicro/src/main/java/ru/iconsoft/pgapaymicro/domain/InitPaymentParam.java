package ru.iconsoft.pgapaymicro.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A InitPaymentParam.
 */
@Entity
@Table(name = "init_payment_param")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InitPaymentParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 32)
    @Column(name = "merchant_id", length = 32, nullable = false)
    private String merchantId;

    @NotNull
    @Size(max = 128)
    @Column(name = "order_id", length = 128, nullable = false)
    private String orderId;

    @Column(name = "user_id")
    private String userId;

    @Size(max = 125)
    @Column(name = "account_id", length = 125)
    private String accountId;

    @Column(name = "ammount", precision = 21, scale = 2)
    private BigDecimal ammount;

    @Column(name = "fee", precision = 21, scale = 2)
    private BigDecimal fee;

    @Column(name = "exponent")
    private Integer exponent;

    @Size(max = 30)
    @Column(name = "purchase_short_desc", length = 30)
    private String purchaseShortDesc;

    @Size(max = 125)
    @Column(name = "purchase_long_desc", length = 125)
    private String purchaseLongDesc;

    @OneToOne
    @JoinColumn(unique = true)
    private Payment payment;

    @OneToMany(mappedBy = "initPaymentParam")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<InitPaymentOrderParam> initPaymentOrderParams = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public InitPaymentParam merchantId(String merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getOrderId() {
        return orderId;
    }

    public InitPaymentParam orderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public InitPaymentParam userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccountId() {
        return accountId;
    }

    public InitPaymentParam accountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public InitPaymentParam ammount(BigDecimal ammount) {
        this.ammount = ammount;
        return this;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public InitPaymentParam fee(BigDecimal fee) {
        this.fee = fee;
        return this;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public Integer getExponent() {
        return exponent;
    }

    public InitPaymentParam exponent(Integer exponent) {
        this.exponent = exponent;
        return this;
    }

    public void setExponent(Integer exponent) {
        this.exponent = exponent;
    }

    public String getPurchaseShortDesc() {
        return purchaseShortDesc;
    }

    public InitPaymentParam purchaseShortDesc(String purchaseShortDesc) {
        this.purchaseShortDesc = purchaseShortDesc;
        return this;
    }

    public void setPurchaseShortDesc(String purchaseShortDesc) {
        this.purchaseShortDesc = purchaseShortDesc;
    }

    public String getPurchaseLongDesc() {
        return purchaseLongDesc;
    }

    public InitPaymentParam purchaseLongDesc(String purchaseLongDesc) {
        this.purchaseLongDesc = purchaseLongDesc;
        return this;
    }

    public void setPurchaseLongDesc(String purchaseLongDesc) {
        this.purchaseLongDesc = purchaseLongDesc;
    }

    public Payment getPayment() {
        return payment;
    }

    public InitPaymentParam payment(Payment payment) {
        this.payment = payment;
        return this;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Set<InitPaymentOrderParam> getInitPaymentOrderParams() {
        return initPaymentOrderParams;
    }

    public InitPaymentParam initPaymentOrderParams(Set<InitPaymentOrderParam> initPaymentOrderParams) {
        this.initPaymentOrderParams = initPaymentOrderParams;
        return this;
    }

    public InitPaymentParam addInitPaymentOrderParam(InitPaymentOrderParam initPaymentOrderParam) {
        this.initPaymentOrderParams.add(initPaymentOrderParam);
        initPaymentOrderParam.setInitPaymentParam(this);
        return this;
    }

    public InitPaymentParam removeInitPaymentOrderParam(InitPaymentOrderParam initPaymentOrderParam) {
        this.initPaymentOrderParams.remove(initPaymentOrderParam);
        initPaymentOrderParam.setInitPaymentParam(null);
        return this;
    }

    public void setInitPaymentOrderParams(Set<InitPaymentOrderParam> initPaymentOrderParams) {
        this.initPaymentOrderParams = initPaymentOrderParams;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InitPaymentParam)) {
            return false;
        }
        return id != null && id.equals(((InitPaymentParam) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "InitPaymentParam{" +
            "id=" + getId() +
            ", merchantId='" + getMerchantId() + "'" +
            ", orderId='" + getOrderId() + "'" +
            ", userId='" + getUserId() + "'" +
            ", accountId='" + getAccountId() + "'" +
            ", ammount=" + getAmmount() +
            ", fee=" + getFee() +
            ", exponent=" + getExponent() +
            ", purchaseShortDesc='" + getPurchaseShortDesc() + "'" +
            ", purchaseLongDesc='" + getPurchaseLongDesc() + "'" +
            "}";
    }
}
