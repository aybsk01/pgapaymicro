package ru.iconsoft.pgapaymicro.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

import ru.iconsoft.pgapaymicro.domain.enumeration.PaymentStatus;

/**
 * A Payment.
 */
@Entity
@Table(name = "payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Payment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private PaymentStatus status;

    @Size(max = 32)
    @Column(name = "pga_trx_id", length = 32)
    private String pgaTrxId;

    @Column(name = "result_code")
    private Integer resultCode;

    @Size(max = 32)
    @Column(name = "ext_result_code", length = 32)
    private String extResultCode;

    @Column(name = "rrn")
    private String rrn;

    @Column(name = "transaction_dt")
    private Instant transactionDt;

    @Size(max = 26)
    @Column(name = "card_holder", length = 26)
    private String cardHolder;

    @Size(max = 20)
    @Column(name = "auth_code", length = 20)
    private String authCode;

    @Size(max = 19)
    @Column(name = "pan", length = 19)
    private String pan;

    @Column(name = "full_auth")
    private Boolean fullAuth;

    @ManyToOne
    @JsonIgnoreProperties("payments")
    private PaymentCard paymentcard;

    @OneToOne(mappedBy = "payment")
    @JsonIgnore
    private InitPaymentParam initPaymentParam;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public Payment status(PaymentStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public String getPgaTrxId() {
        return pgaTrxId;
    }

    public Payment pgaTrxId(String pgaTrxId) {
        this.pgaTrxId = pgaTrxId;
        return this;
    }

    public void setPgaTrxId(String pgaTrxId) {
        this.pgaTrxId = pgaTrxId;
    }

    public Integer getResultCode() {
        return resultCode;
    }

    public Payment resultCode(Integer resultCode) {
        this.resultCode = resultCode;
        return this;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getExtResultCode() {
        return extResultCode;
    }

    public Payment extResultCode(String extResultCode) {
        this.extResultCode = extResultCode;
        return this;
    }

    public void setExtResultCode(String extResultCode) {
        this.extResultCode = extResultCode;
    }

    public String getRrn() {
        return rrn;
    }

    public Payment rrn(String rrn) {
        this.rrn = rrn;
        return this;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public Instant getTransactionDt() {
        return transactionDt;
    }

    public Payment transactionDt(Instant transactionDt) {
        this.transactionDt = transactionDt;
        return this;
    }

    public void setTransactionDt(Instant transactionDt) {
        this.transactionDt = transactionDt;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public Payment cardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
        return this;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getAuthCode() {
        return authCode;
    }

    public Payment authCode(String authCode) {
        this.authCode = authCode;
        return this;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getPan() {
        return pan;
    }

    public Payment pan(String pan) {
        this.pan = pan;
        return this;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Boolean isFullAuth() {
        return fullAuth;
    }

    public Payment fullAuth(Boolean fullAuth) {
        this.fullAuth = fullAuth;
        return this;
    }

    public void setFullAuth(Boolean fullAuth) {
        this.fullAuth = fullAuth;
    }

    public PaymentCard getPaymentcard() {
        return paymentcard;
    }

    public Payment paymentcard(PaymentCard paymentCard) {
        this.paymentcard = paymentCard;
        return this;
    }

    public void setPaymentcard(PaymentCard paymentCard) {
        this.paymentcard = paymentCard;
    }

    public InitPaymentParam getInitPaymentParam() {
        return initPaymentParam;
    }

    public Payment initPaymentParam(InitPaymentParam initPaymentParam) {
        this.initPaymentParam = initPaymentParam;
        return this;
    }

    public void setInitPaymentParam(InitPaymentParam initPaymentParam) {
        this.initPaymentParam = initPaymentParam;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Payment)) {
            return false;
        }
        return id != null && id.equals(((Payment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Payment{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", pgaTrxId='" + getPgaTrxId() + "'" +
            ", resultCode=" + getResultCode() +
            ", extResultCode='" + getExtResultCode() + "'" +
            ", rrn='" + getRrn() + "'" +
            ", transactionDt='" + getTransactionDt() + "'" +
            ", cardHolder='" + getCardHolder() + "'" +
            ", authCode='" + getAuthCode() + "'" +
            ", pan='" + getPan() + "'" +
            ", fullAuth='" + isFullAuth() + "'" +
            "}";
    }
}
