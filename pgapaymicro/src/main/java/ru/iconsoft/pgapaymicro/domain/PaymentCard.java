package ru.iconsoft.pgapaymicro.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A PaymentCard.
 */
@Entity
@Table(name = "payment_card")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PaymentCard implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private String userId;

    @Size(max = 32)
    @Column(name = "card_id", length = 32)
    private String cardId;

    @Size(max = 4)
    @Column(name = "expire_dt", length = 4)
    private String expireDt;

    @Column(name = "requrrent")
    private Boolean requrrent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public PaymentCard userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCardId() {
        return cardId;
    }

    public PaymentCard cardId(String cardId) {
        this.cardId = cardId;
        return this;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getExpireDt() {
        return expireDt;
    }

    public PaymentCard expireDt(String expireDt) {
        this.expireDt = expireDt;
        return this;
    }

    public void setExpireDt(String expireDt) {
        this.expireDt = expireDt;
    }

    public Boolean isRequrrent() {
        return requrrent;
    }

    public PaymentCard requrrent(Boolean requrrent) {
        this.requrrent = requrrent;
        return this;
    }

    public void setRequrrent(Boolean requrrent) {
        this.requrrent = requrrent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentCard)) {
            return false;
        }
        return id != null && id.equals(((PaymentCard) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PaymentCard{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", cardId='" + getCardId() + "'" +
            ", expireDt='" + getExpireDt() + "'" +
            ", requrrent='" + isRequrrent() + "'" +
            "}";
    }
}
