package ru.iconsoft.pgapaymicro.web.rest;

import ru.iconsoft.pgapaymicro.PgapaymicroApp;
import ru.iconsoft.pgapaymicro.domain.Payment;
import ru.iconsoft.pgapaymicro.repository.PaymentRepository;
import ru.iconsoft.pgapaymicro.service.PaymentService;
import ru.iconsoft.pgapaymicro.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static ru.iconsoft.pgapaymicro.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ru.iconsoft.pgapaymicro.domain.enumeration.PaymentStatus;
/**
 * Integration tests for the {@link PaymentResource} REST controller.
 */
@SpringBootTest(classes = PgapaymicroApp.class)
public class PaymentResourceIT {

    private static final PaymentStatus DEFAULT_STATUS = PaymentStatus.NEW;
    private static final PaymentStatus UPDATED_STATUS = PaymentStatus.COMPLETE;

    private static final String DEFAULT_PGA_TRX_ID = "AAAAAAAAAA";
    private static final String UPDATED_PGA_TRX_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_RESULT_CODE = 1;
    private static final Integer UPDATED_RESULT_CODE = 2;

    private static final String DEFAULT_EXT_RESULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EXT_RESULT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_RRN = "AAAAAAAAAA";
    private static final String UPDATED_RRN = "BBBBBBBBBB";

    private static final Instant DEFAULT_TRANSACTION_DT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TRANSACTION_DT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CARD_HOLDER = "AAAAAAAAAA";
    private static final String UPDATED_CARD_HOLDER = "BBBBBBBBBB";

    private static final String DEFAULT_AUTH_CODE = "AAAAAAAAAA";
    private static final String UPDATED_AUTH_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PAN = "AAAAAAAAAA";
    private static final String UPDATED_PAN = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FULL_AUTH = false;
    private static final Boolean UPDATED_FULL_AUTH = true;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPaymentMockMvc;

    private Payment payment;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PaymentResource paymentResource = new PaymentResource(paymentService);
        this.restPaymentMockMvc = MockMvcBuilders.standaloneSetup(paymentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createEntity(EntityManager em) {
        Payment payment = new Payment()
            .status(DEFAULT_STATUS)
            .pgaTrxId(DEFAULT_PGA_TRX_ID)
            .resultCode(DEFAULT_RESULT_CODE)
            .extResultCode(DEFAULT_EXT_RESULT_CODE)
            .rrn(DEFAULT_RRN)
            .transactionDt(DEFAULT_TRANSACTION_DT)
            .cardHolder(DEFAULT_CARD_HOLDER)
            .authCode(DEFAULT_AUTH_CODE)
            .pan(DEFAULT_PAN)
            .fullAuth(DEFAULT_FULL_AUTH);
        return payment;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createUpdatedEntity(EntityManager em) {
        Payment payment = new Payment()
            .status(UPDATED_STATUS)
            .pgaTrxId(UPDATED_PGA_TRX_ID)
            .resultCode(UPDATED_RESULT_CODE)
            .extResultCode(UPDATED_EXT_RESULT_CODE)
            .rrn(UPDATED_RRN)
            .transactionDt(UPDATED_TRANSACTION_DT)
            .cardHolder(UPDATED_CARD_HOLDER)
            .authCode(UPDATED_AUTH_CODE)
            .pan(UPDATED_PAN)
            .fullAuth(UPDATED_FULL_AUTH);
        return payment;
    }

    @BeforeEach
    public void initTest() {
        payment = createEntity(em);
    }

    @Test
    @Transactional
    public void createPayment() throws Exception {
        int databaseSizeBeforeCreate = paymentRepository.findAll().size();

        // Create the Payment
        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isCreated());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate + 1);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPayment.getPgaTrxId()).isEqualTo(DEFAULT_PGA_TRX_ID);
        assertThat(testPayment.getResultCode()).isEqualTo(DEFAULT_RESULT_CODE);
        assertThat(testPayment.getExtResultCode()).isEqualTo(DEFAULT_EXT_RESULT_CODE);
        assertThat(testPayment.getRrn()).isEqualTo(DEFAULT_RRN);
        assertThat(testPayment.getTransactionDt()).isEqualTo(DEFAULT_TRANSACTION_DT);
        assertThat(testPayment.getCardHolder()).isEqualTo(DEFAULT_CARD_HOLDER);
        assertThat(testPayment.getAuthCode()).isEqualTo(DEFAULT_AUTH_CODE);
        assertThat(testPayment.getPan()).isEqualTo(DEFAULT_PAN);
        assertThat(testPayment.isFullAuth()).isEqualTo(DEFAULT_FULL_AUTH);
    }

    @Test
    @Transactional
    public void createPaymentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentRepository.findAll().size();

        // Create the Payment with an existing ID
        payment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentRepository.findAll().size();
        // set the field null
        payment.setStatus(null);

        // Create the Payment, which fails.

        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPayments() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList
        restPaymentMockMvc.perform(get("/api/payments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payment.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].pgaTrxId").value(hasItem(DEFAULT_PGA_TRX_ID)))
            .andExpect(jsonPath("$.[*].resultCode").value(hasItem(DEFAULT_RESULT_CODE)))
            .andExpect(jsonPath("$.[*].extResultCode").value(hasItem(DEFAULT_EXT_RESULT_CODE)))
            .andExpect(jsonPath("$.[*].rrn").value(hasItem(DEFAULT_RRN)))
            .andExpect(jsonPath("$.[*].transactionDt").value(hasItem(DEFAULT_TRANSACTION_DT.toString())))
            .andExpect(jsonPath("$.[*].cardHolder").value(hasItem(DEFAULT_CARD_HOLDER)))
            .andExpect(jsonPath("$.[*].authCode").value(hasItem(DEFAULT_AUTH_CODE)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].fullAuth").value(hasItem(DEFAULT_FULL_AUTH.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get the payment
        restPaymentMockMvc.perform(get("/api/payments/{id}", payment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(payment.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.pgaTrxId").value(DEFAULT_PGA_TRX_ID))
            .andExpect(jsonPath("$.resultCode").value(DEFAULT_RESULT_CODE))
            .andExpect(jsonPath("$.extResultCode").value(DEFAULT_EXT_RESULT_CODE))
            .andExpect(jsonPath("$.rrn").value(DEFAULT_RRN))
            .andExpect(jsonPath("$.transactionDt").value(DEFAULT_TRANSACTION_DT.toString()))
            .andExpect(jsonPath("$.cardHolder").value(DEFAULT_CARD_HOLDER))
            .andExpect(jsonPath("$.authCode").value(DEFAULT_AUTH_CODE))
            .andExpect(jsonPath("$.pan").value(DEFAULT_PAN))
            .andExpect(jsonPath("$.fullAuth").value(DEFAULT_FULL_AUTH.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPayment() throws Exception {
        // Get the payment
        restPaymentMockMvc.perform(get("/api/payments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePayment() throws Exception {
        // Initialize the database
        paymentService.save(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment
        Payment updatedPayment = paymentRepository.findById(payment.getId()).get();
        // Disconnect from session so that the updates on updatedPayment are not directly saved in db
        em.detach(updatedPayment);
        updatedPayment
            .status(UPDATED_STATUS)
            .pgaTrxId(UPDATED_PGA_TRX_ID)
            .resultCode(UPDATED_RESULT_CODE)
            .extResultCode(UPDATED_EXT_RESULT_CODE)
            .rrn(UPDATED_RRN)
            .transactionDt(UPDATED_TRANSACTION_DT)
            .cardHolder(UPDATED_CARD_HOLDER)
            .authCode(UPDATED_AUTH_CODE)
            .pan(UPDATED_PAN)
            .fullAuth(UPDATED_FULL_AUTH);

        restPaymentMockMvc.perform(put("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPayment)))
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPayment.getPgaTrxId()).isEqualTo(UPDATED_PGA_TRX_ID);
        assertThat(testPayment.getResultCode()).isEqualTo(UPDATED_RESULT_CODE);
        assertThat(testPayment.getExtResultCode()).isEqualTo(UPDATED_EXT_RESULT_CODE);
        assertThat(testPayment.getRrn()).isEqualTo(UPDATED_RRN);
        assertThat(testPayment.getTransactionDt()).isEqualTo(UPDATED_TRANSACTION_DT);
        assertThat(testPayment.getCardHolder()).isEqualTo(UPDATED_CARD_HOLDER);
        assertThat(testPayment.getAuthCode()).isEqualTo(UPDATED_AUTH_CODE);
        assertThat(testPayment.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testPayment.isFullAuth()).isEqualTo(UPDATED_FULL_AUTH);
    }

    @Test
    @Transactional
    public void updateNonExistingPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Create the Payment

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentMockMvc.perform(put("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePayment() throws Exception {
        // Initialize the database
        paymentService.save(payment);

        int databaseSizeBeforeDelete = paymentRepository.findAll().size();

        // Delete the payment
        restPaymentMockMvc.perform(delete("/api/payments/{id}", payment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Payment.class);
        Payment payment1 = new Payment();
        payment1.setId(1L);
        Payment payment2 = new Payment();
        payment2.setId(payment1.getId());
        assertThat(payment1).isEqualTo(payment2);
        payment2.setId(2L);
        assertThat(payment1).isNotEqualTo(payment2);
        payment1.setId(null);
        assertThat(payment1).isNotEqualTo(payment2);
    }
}
