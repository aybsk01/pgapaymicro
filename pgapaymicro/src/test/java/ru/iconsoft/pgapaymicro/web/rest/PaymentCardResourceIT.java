package ru.iconsoft.pgapaymicro.web.rest;

import ru.iconsoft.pgapaymicro.PgapaymicroApp;
import ru.iconsoft.pgapaymicro.domain.PaymentCard;
import ru.iconsoft.pgapaymicro.repository.PaymentCardRepository;
import ru.iconsoft.pgapaymicro.service.PaymentCardService;
import ru.iconsoft.pgapaymicro.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.iconsoft.pgapaymicro.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PaymentCardResource} REST controller.
 */
@SpringBootTest(classes = PgapaymicroApp.class)
public class PaymentCardResourceIT {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CARD_ID = "AAAAAAAAAA";
    private static final String UPDATED_CARD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EXPIRE_DT = "AAAA";
    private static final String UPDATED_EXPIRE_DT = "BBBB";

    private static final Boolean DEFAULT_REQURRENT = false;
    private static final Boolean UPDATED_REQURRENT = true;

    @Autowired
    private PaymentCardRepository paymentCardRepository;

    @Autowired
    private PaymentCardService paymentCardService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPaymentCardMockMvc;

    private PaymentCard paymentCard;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PaymentCardResource paymentCardResource = new PaymentCardResource(paymentCardService);
        this.restPaymentCardMockMvc = MockMvcBuilders.standaloneSetup(paymentCardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentCard createEntity(EntityManager em) {
        PaymentCard paymentCard = new PaymentCard()
            .userId(DEFAULT_USER_ID)
            .cardId(DEFAULT_CARD_ID)
            .expireDt(DEFAULT_EXPIRE_DT)
            .requrrent(DEFAULT_REQURRENT);
        return paymentCard;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentCard createUpdatedEntity(EntityManager em) {
        PaymentCard paymentCard = new PaymentCard()
            .userId(UPDATED_USER_ID)
            .cardId(UPDATED_CARD_ID)
            .expireDt(UPDATED_EXPIRE_DT)
            .requrrent(UPDATED_REQURRENT);
        return paymentCard;
    }

    @BeforeEach
    public void initTest() {
        paymentCard = createEntity(em);
    }

    @Test
    @Transactional
    public void createPaymentCard() throws Exception {
        int databaseSizeBeforeCreate = paymentCardRepository.findAll().size();

        // Create the PaymentCard
        restPaymentCardMockMvc.perform(post("/api/payment-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentCard)))
            .andExpect(status().isCreated());

        // Validate the PaymentCard in the database
        List<PaymentCard> paymentCardList = paymentCardRepository.findAll();
        assertThat(paymentCardList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentCard testPaymentCard = paymentCardList.get(paymentCardList.size() - 1);
        assertThat(testPaymentCard.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testPaymentCard.getCardId()).isEqualTo(DEFAULT_CARD_ID);
        assertThat(testPaymentCard.getExpireDt()).isEqualTo(DEFAULT_EXPIRE_DT);
        assertThat(testPaymentCard.isRequrrent()).isEqualTo(DEFAULT_REQURRENT);
    }

    @Test
    @Transactional
    public void createPaymentCardWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentCardRepository.findAll().size();

        // Create the PaymentCard with an existing ID
        paymentCard.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentCardMockMvc.perform(post("/api/payment-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentCard)))
            .andExpect(status().isBadRequest());

        // Validate the PaymentCard in the database
        List<PaymentCard> paymentCardList = paymentCardRepository.findAll();
        assertThat(paymentCardList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPaymentCards() throws Exception {
        // Initialize the database
        paymentCardRepository.saveAndFlush(paymentCard);

        // Get all the paymentCardList
        restPaymentCardMockMvc.perform(get("/api/payment-cards?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentCard.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].cardId").value(hasItem(DEFAULT_CARD_ID)))
            .andExpect(jsonPath("$.[*].expireDt").value(hasItem(DEFAULT_EXPIRE_DT)))
            .andExpect(jsonPath("$.[*].requrrent").value(hasItem(DEFAULT_REQURRENT.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPaymentCard() throws Exception {
        // Initialize the database
        paymentCardRepository.saveAndFlush(paymentCard);

        // Get the paymentCard
        restPaymentCardMockMvc.perform(get("/api/payment-cards/{id}", paymentCard.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paymentCard.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.cardId").value(DEFAULT_CARD_ID))
            .andExpect(jsonPath("$.expireDt").value(DEFAULT_EXPIRE_DT))
            .andExpect(jsonPath("$.requrrent").value(DEFAULT_REQURRENT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPaymentCard() throws Exception {
        // Get the paymentCard
        restPaymentCardMockMvc.perform(get("/api/payment-cards/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePaymentCard() throws Exception {
        // Initialize the database
        paymentCardService.save(paymentCard);

        int databaseSizeBeforeUpdate = paymentCardRepository.findAll().size();

        // Update the paymentCard
        PaymentCard updatedPaymentCard = paymentCardRepository.findById(paymentCard.getId()).get();
        // Disconnect from session so that the updates on updatedPaymentCard are not directly saved in db
        em.detach(updatedPaymentCard);
        updatedPaymentCard
            .userId(UPDATED_USER_ID)
            .cardId(UPDATED_CARD_ID)
            .expireDt(UPDATED_EXPIRE_DT)
            .requrrent(UPDATED_REQURRENT);

        restPaymentCardMockMvc.perform(put("/api/payment-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPaymentCard)))
            .andExpect(status().isOk());

        // Validate the PaymentCard in the database
        List<PaymentCard> paymentCardList = paymentCardRepository.findAll();
        assertThat(paymentCardList).hasSize(databaseSizeBeforeUpdate);
        PaymentCard testPaymentCard = paymentCardList.get(paymentCardList.size() - 1);
        assertThat(testPaymentCard.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testPaymentCard.getCardId()).isEqualTo(UPDATED_CARD_ID);
        assertThat(testPaymentCard.getExpireDt()).isEqualTo(UPDATED_EXPIRE_DT);
        assertThat(testPaymentCard.isRequrrent()).isEqualTo(UPDATED_REQURRENT);
    }

    @Test
    @Transactional
    public void updateNonExistingPaymentCard() throws Exception {
        int databaseSizeBeforeUpdate = paymentCardRepository.findAll().size();

        // Create the PaymentCard

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentCardMockMvc.perform(put("/api/payment-cards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentCard)))
            .andExpect(status().isBadRequest());

        // Validate the PaymentCard in the database
        List<PaymentCard> paymentCardList = paymentCardRepository.findAll();
        assertThat(paymentCardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePaymentCard() throws Exception {
        // Initialize the database
        paymentCardService.save(paymentCard);

        int databaseSizeBeforeDelete = paymentCardRepository.findAll().size();

        // Delete the paymentCard
        restPaymentCardMockMvc.perform(delete("/api/payment-cards/{id}", paymentCard.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PaymentCard> paymentCardList = paymentCardRepository.findAll();
        assertThat(paymentCardList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentCard.class);
        PaymentCard paymentCard1 = new PaymentCard();
        paymentCard1.setId(1L);
        PaymentCard paymentCard2 = new PaymentCard();
        paymentCard2.setId(paymentCard1.getId());
        assertThat(paymentCard1).isEqualTo(paymentCard2);
        paymentCard2.setId(2L);
        assertThat(paymentCard1).isNotEqualTo(paymentCard2);
        paymentCard1.setId(null);
        assertThat(paymentCard1).isNotEqualTo(paymentCard2);
    }
}
