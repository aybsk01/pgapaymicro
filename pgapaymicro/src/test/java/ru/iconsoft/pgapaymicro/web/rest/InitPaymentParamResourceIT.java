package ru.iconsoft.pgapaymicro.web.rest;

import ru.iconsoft.pgapaymicro.PgapaymicroApp;
import ru.iconsoft.pgapaymicro.domain.InitPaymentParam;
import ru.iconsoft.pgapaymicro.repository.InitPaymentParamRepository;
import ru.iconsoft.pgapaymicro.service.InitPaymentParamService;
import ru.iconsoft.pgapaymicro.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static ru.iconsoft.pgapaymicro.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link InitPaymentParamResource} REST controller.
 */
@SpringBootTest(classes = PgapaymicroApp.class)
public class InitPaymentParamResourceIT {

    private static final String DEFAULT_MERCHANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ORDER_ID = "AAAAAAAAAA";
    private static final String UPDATED_ORDER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_ID = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_ID = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_AMMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMMOUNT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_FEE = new BigDecimal(1);
    private static final BigDecimal UPDATED_FEE = new BigDecimal(2);

    private static final Integer DEFAULT_EXPONENT = 1;
    private static final Integer UPDATED_EXPONENT = 2;

    private static final String DEFAULT_PURCHASE_SHORT_DESC = "AAAAAAAAAA";
    private static final String UPDATED_PURCHASE_SHORT_DESC = "BBBBBBBBBB";

    private static final String DEFAULT_PURCHASE_LONG_DESC = "AAAAAAAAAA";
    private static final String UPDATED_PURCHASE_LONG_DESC = "BBBBBBBBBB";

    @Autowired
    private InitPaymentParamRepository initPaymentParamRepository;

    @Autowired
    private InitPaymentParamService initPaymentParamService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restInitPaymentParamMockMvc;

    private InitPaymentParam initPaymentParam;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InitPaymentParamResource initPaymentParamResource = new InitPaymentParamResource(initPaymentParamService);
        this.restInitPaymentParamMockMvc = MockMvcBuilders.standaloneSetup(initPaymentParamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InitPaymentParam createEntity(EntityManager em) {
        InitPaymentParam initPaymentParam = new InitPaymentParam()
            .merchantId(DEFAULT_MERCHANT_ID)
            .orderId(DEFAULT_ORDER_ID)
            .userId(DEFAULT_USER_ID)
            .accountId(DEFAULT_ACCOUNT_ID)
            .ammount(DEFAULT_AMMOUNT)
            .fee(DEFAULT_FEE)
            .exponent(DEFAULT_EXPONENT)
            .purchaseShortDesc(DEFAULT_PURCHASE_SHORT_DESC)
            .purchaseLongDesc(DEFAULT_PURCHASE_LONG_DESC);
        return initPaymentParam;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InitPaymentParam createUpdatedEntity(EntityManager em) {
        InitPaymentParam initPaymentParam = new InitPaymentParam()
            .merchantId(UPDATED_MERCHANT_ID)
            .orderId(UPDATED_ORDER_ID)
            .userId(UPDATED_USER_ID)
            .accountId(UPDATED_ACCOUNT_ID)
            .ammount(UPDATED_AMMOUNT)
            .fee(UPDATED_FEE)
            .exponent(UPDATED_EXPONENT)
            .purchaseShortDesc(UPDATED_PURCHASE_SHORT_DESC)
            .purchaseLongDesc(UPDATED_PURCHASE_LONG_DESC);
        return initPaymentParam;
    }

    @BeforeEach
    public void initTest() {
        initPaymentParam = createEntity(em);
    }

    @Test
    @Transactional
    public void createInitPaymentParam() throws Exception {
        int databaseSizeBeforeCreate = initPaymentParamRepository.findAll().size();

        // Create the InitPaymentParam
        restInitPaymentParamMockMvc.perform(post("/api/init-payment-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(initPaymentParam)))
            .andExpect(status().isCreated());

        // Validate the InitPaymentParam in the database
        List<InitPaymentParam> initPaymentParamList = initPaymentParamRepository.findAll();
        assertThat(initPaymentParamList).hasSize(databaseSizeBeforeCreate + 1);
        InitPaymentParam testInitPaymentParam = initPaymentParamList.get(initPaymentParamList.size() - 1);
        assertThat(testInitPaymentParam.getMerchantId()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testInitPaymentParam.getOrderId()).isEqualTo(DEFAULT_ORDER_ID);
        assertThat(testInitPaymentParam.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testInitPaymentParam.getAccountId()).isEqualTo(DEFAULT_ACCOUNT_ID);
        assertThat(testInitPaymentParam.getAmmount()).isEqualTo(DEFAULT_AMMOUNT);
        assertThat(testInitPaymentParam.getFee()).isEqualTo(DEFAULT_FEE);
        assertThat(testInitPaymentParam.getExponent()).isEqualTo(DEFAULT_EXPONENT);
        assertThat(testInitPaymentParam.getPurchaseShortDesc()).isEqualTo(DEFAULT_PURCHASE_SHORT_DESC);
        assertThat(testInitPaymentParam.getPurchaseLongDesc()).isEqualTo(DEFAULT_PURCHASE_LONG_DESC);
    }

    @Test
    @Transactional
    public void createInitPaymentParamWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = initPaymentParamRepository.findAll().size();

        // Create the InitPaymentParam with an existing ID
        initPaymentParam.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInitPaymentParamMockMvc.perform(post("/api/init-payment-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(initPaymentParam)))
            .andExpect(status().isBadRequest());

        // Validate the InitPaymentParam in the database
        List<InitPaymentParam> initPaymentParamList = initPaymentParamRepository.findAll();
        assertThat(initPaymentParamList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMerchantIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = initPaymentParamRepository.findAll().size();
        // set the field null
        initPaymentParam.setMerchantId(null);

        // Create the InitPaymentParam, which fails.

        restInitPaymentParamMockMvc.perform(post("/api/init-payment-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(initPaymentParam)))
            .andExpect(status().isBadRequest());

        List<InitPaymentParam> initPaymentParamList = initPaymentParamRepository.findAll();
        assertThat(initPaymentParamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrderIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = initPaymentParamRepository.findAll().size();
        // set the field null
        initPaymentParam.setOrderId(null);

        // Create the InitPaymentParam, which fails.

        restInitPaymentParamMockMvc.perform(post("/api/init-payment-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(initPaymentParam)))
            .andExpect(status().isBadRequest());

        List<InitPaymentParam> initPaymentParamList = initPaymentParamRepository.findAll();
        assertThat(initPaymentParamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInitPaymentParams() throws Exception {
        // Initialize the database
        initPaymentParamRepository.saveAndFlush(initPaymentParam);

        // Get all the initPaymentParamList
        restInitPaymentParamMockMvc.perform(get("/api/init-payment-params?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(initPaymentParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].merchantId").value(hasItem(DEFAULT_MERCHANT_ID)))
            .andExpect(jsonPath("$.[*].orderId").value(hasItem(DEFAULT_ORDER_ID)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].accountId").value(hasItem(DEFAULT_ACCOUNT_ID)))
            .andExpect(jsonPath("$.[*].ammount").value(hasItem(DEFAULT_AMMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].fee").value(hasItem(DEFAULT_FEE.intValue())))
            .andExpect(jsonPath("$.[*].exponent").value(hasItem(DEFAULT_EXPONENT)))
            .andExpect(jsonPath("$.[*].purchaseShortDesc").value(hasItem(DEFAULT_PURCHASE_SHORT_DESC)))
            .andExpect(jsonPath("$.[*].purchaseLongDesc").value(hasItem(DEFAULT_PURCHASE_LONG_DESC)));
    }
    
    @Test
    @Transactional
    public void getInitPaymentParam() throws Exception {
        // Initialize the database
        initPaymentParamRepository.saveAndFlush(initPaymentParam);

        // Get the initPaymentParam
        restInitPaymentParamMockMvc.perform(get("/api/init-payment-params/{id}", initPaymentParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(initPaymentParam.getId().intValue()))
            .andExpect(jsonPath("$.merchantId").value(DEFAULT_MERCHANT_ID))
            .andExpect(jsonPath("$.orderId").value(DEFAULT_ORDER_ID))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.accountId").value(DEFAULT_ACCOUNT_ID))
            .andExpect(jsonPath("$.ammount").value(DEFAULT_AMMOUNT.intValue()))
            .andExpect(jsonPath("$.fee").value(DEFAULT_FEE.intValue()))
            .andExpect(jsonPath("$.exponent").value(DEFAULT_EXPONENT))
            .andExpect(jsonPath("$.purchaseShortDesc").value(DEFAULT_PURCHASE_SHORT_DESC))
            .andExpect(jsonPath("$.purchaseLongDesc").value(DEFAULT_PURCHASE_LONG_DESC));
    }

    @Test
    @Transactional
    public void getNonExistingInitPaymentParam() throws Exception {
        // Get the initPaymentParam
        restInitPaymentParamMockMvc.perform(get("/api/init-payment-params/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInitPaymentParam() throws Exception {
        // Initialize the database
        initPaymentParamService.save(initPaymentParam);

        int databaseSizeBeforeUpdate = initPaymentParamRepository.findAll().size();

        // Update the initPaymentParam
        InitPaymentParam updatedInitPaymentParam = initPaymentParamRepository.findById(initPaymentParam.getId()).get();
        // Disconnect from session so that the updates on updatedInitPaymentParam are not directly saved in db
        em.detach(updatedInitPaymentParam);
        updatedInitPaymentParam
            .merchantId(UPDATED_MERCHANT_ID)
            .orderId(UPDATED_ORDER_ID)
            .userId(UPDATED_USER_ID)
            .accountId(UPDATED_ACCOUNT_ID)
            .ammount(UPDATED_AMMOUNT)
            .fee(UPDATED_FEE)
            .exponent(UPDATED_EXPONENT)
            .purchaseShortDesc(UPDATED_PURCHASE_SHORT_DESC)
            .purchaseLongDesc(UPDATED_PURCHASE_LONG_DESC);

        restInitPaymentParamMockMvc.perform(put("/api/init-payment-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInitPaymentParam)))
            .andExpect(status().isOk());

        // Validate the InitPaymentParam in the database
        List<InitPaymentParam> initPaymentParamList = initPaymentParamRepository.findAll();
        assertThat(initPaymentParamList).hasSize(databaseSizeBeforeUpdate);
        InitPaymentParam testInitPaymentParam = initPaymentParamList.get(initPaymentParamList.size() - 1);
        assertThat(testInitPaymentParam.getMerchantId()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testInitPaymentParam.getOrderId()).isEqualTo(UPDATED_ORDER_ID);
        assertThat(testInitPaymentParam.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testInitPaymentParam.getAccountId()).isEqualTo(UPDATED_ACCOUNT_ID);
        assertThat(testInitPaymentParam.getAmmount()).isEqualTo(UPDATED_AMMOUNT);
        assertThat(testInitPaymentParam.getFee()).isEqualTo(UPDATED_FEE);
        assertThat(testInitPaymentParam.getExponent()).isEqualTo(UPDATED_EXPONENT);
        assertThat(testInitPaymentParam.getPurchaseShortDesc()).isEqualTo(UPDATED_PURCHASE_SHORT_DESC);
        assertThat(testInitPaymentParam.getPurchaseLongDesc()).isEqualTo(UPDATED_PURCHASE_LONG_DESC);
    }

    @Test
    @Transactional
    public void updateNonExistingInitPaymentParam() throws Exception {
        int databaseSizeBeforeUpdate = initPaymentParamRepository.findAll().size();

        // Create the InitPaymentParam

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInitPaymentParamMockMvc.perform(put("/api/init-payment-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(initPaymentParam)))
            .andExpect(status().isBadRequest());

        // Validate the InitPaymentParam in the database
        List<InitPaymentParam> initPaymentParamList = initPaymentParamRepository.findAll();
        assertThat(initPaymentParamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInitPaymentParam() throws Exception {
        // Initialize the database
        initPaymentParamService.save(initPaymentParam);

        int databaseSizeBeforeDelete = initPaymentParamRepository.findAll().size();

        // Delete the initPaymentParam
        restInitPaymentParamMockMvc.perform(delete("/api/init-payment-params/{id}", initPaymentParam.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<InitPaymentParam> initPaymentParamList = initPaymentParamRepository.findAll();
        assertThat(initPaymentParamList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InitPaymentParam.class);
        InitPaymentParam initPaymentParam1 = new InitPaymentParam();
        initPaymentParam1.setId(1L);
        InitPaymentParam initPaymentParam2 = new InitPaymentParam();
        initPaymentParam2.setId(initPaymentParam1.getId());
        assertThat(initPaymentParam1).isEqualTo(initPaymentParam2);
        initPaymentParam2.setId(2L);
        assertThat(initPaymentParam1).isNotEqualTo(initPaymentParam2);
        initPaymentParam1.setId(null);
        assertThat(initPaymentParam1).isNotEqualTo(initPaymentParam2);
    }
}
