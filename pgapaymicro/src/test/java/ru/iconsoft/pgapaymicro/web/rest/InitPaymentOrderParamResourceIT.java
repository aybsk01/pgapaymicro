package ru.iconsoft.pgapaymicro.web.rest;

import ru.iconsoft.pgapaymicro.PgapaymicroApp;
import ru.iconsoft.pgapaymicro.domain.InitPaymentOrderParam;
import ru.iconsoft.pgapaymicro.repository.InitPaymentOrderParamRepository;
import ru.iconsoft.pgapaymicro.service.InitPaymentOrderParamService;
import ru.iconsoft.pgapaymicro.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.iconsoft.pgapaymicro.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link InitPaymentOrderParamResource} REST controller.
 */
@SpringBootTest(classes = PgapaymicroApp.class)
public class InitPaymentOrderParamResourceIT {

    private static final String DEFAULT_PARAM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PARAM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PARAM_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_PARAM_VALUE = "BBBBBBBBBB";

    @Autowired
    private InitPaymentOrderParamRepository initPaymentOrderParamRepository;

    @Autowired
    private InitPaymentOrderParamService initPaymentOrderParamService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restInitPaymentOrderParamMockMvc;

    private InitPaymentOrderParam initPaymentOrderParam;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InitPaymentOrderParamResource initPaymentOrderParamResource = new InitPaymentOrderParamResource(initPaymentOrderParamService);
        this.restInitPaymentOrderParamMockMvc = MockMvcBuilders.standaloneSetup(initPaymentOrderParamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InitPaymentOrderParam createEntity(EntityManager em) {
        InitPaymentOrderParam initPaymentOrderParam = new InitPaymentOrderParam()
            .paramName(DEFAULT_PARAM_NAME)
            .paramValue(DEFAULT_PARAM_VALUE);
        return initPaymentOrderParam;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InitPaymentOrderParam createUpdatedEntity(EntityManager em) {
        InitPaymentOrderParam initPaymentOrderParam = new InitPaymentOrderParam()
            .paramName(UPDATED_PARAM_NAME)
            .paramValue(UPDATED_PARAM_VALUE);
        return initPaymentOrderParam;
    }

    @BeforeEach
    public void initTest() {
        initPaymentOrderParam = createEntity(em);
    }

    @Test
    @Transactional
    public void createInitPaymentOrderParam() throws Exception {
        int databaseSizeBeforeCreate = initPaymentOrderParamRepository.findAll().size();

        // Create the InitPaymentOrderParam
        restInitPaymentOrderParamMockMvc.perform(post("/api/init-payment-order-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(initPaymentOrderParam)))
            .andExpect(status().isCreated());

        // Validate the InitPaymentOrderParam in the database
        List<InitPaymentOrderParam> initPaymentOrderParamList = initPaymentOrderParamRepository.findAll();
        assertThat(initPaymentOrderParamList).hasSize(databaseSizeBeforeCreate + 1);
        InitPaymentOrderParam testInitPaymentOrderParam = initPaymentOrderParamList.get(initPaymentOrderParamList.size() - 1);
        assertThat(testInitPaymentOrderParam.getParamName()).isEqualTo(DEFAULT_PARAM_NAME);
        assertThat(testInitPaymentOrderParam.getParamValue()).isEqualTo(DEFAULT_PARAM_VALUE);
    }

    @Test
    @Transactional
    public void createInitPaymentOrderParamWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = initPaymentOrderParamRepository.findAll().size();

        // Create the InitPaymentOrderParam with an existing ID
        initPaymentOrderParam.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInitPaymentOrderParamMockMvc.perform(post("/api/init-payment-order-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(initPaymentOrderParam)))
            .andExpect(status().isBadRequest());

        // Validate the InitPaymentOrderParam in the database
        List<InitPaymentOrderParam> initPaymentOrderParamList = initPaymentOrderParamRepository.findAll();
        assertThat(initPaymentOrderParamList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllInitPaymentOrderParams() throws Exception {
        // Initialize the database
        initPaymentOrderParamRepository.saveAndFlush(initPaymentOrderParam);

        // Get all the initPaymentOrderParamList
        restInitPaymentOrderParamMockMvc.perform(get("/api/init-payment-order-params?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(initPaymentOrderParam.getId().intValue())))
            .andExpect(jsonPath("$.[*].paramName").value(hasItem(DEFAULT_PARAM_NAME)))
            .andExpect(jsonPath("$.[*].paramValue").value(hasItem(DEFAULT_PARAM_VALUE)));
    }
    
    @Test
    @Transactional
    public void getInitPaymentOrderParam() throws Exception {
        // Initialize the database
        initPaymentOrderParamRepository.saveAndFlush(initPaymentOrderParam);

        // Get the initPaymentOrderParam
        restInitPaymentOrderParamMockMvc.perform(get("/api/init-payment-order-params/{id}", initPaymentOrderParam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(initPaymentOrderParam.getId().intValue()))
            .andExpect(jsonPath("$.paramName").value(DEFAULT_PARAM_NAME))
            .andExpect(jsonPath("$.paramValue").value(DEFAULT_PARAM_VALUE));
    }

    @Test
    @Transactional
    public void getNonExistingInitPaymentOrderParam() throws Exception {
        // Get the initPaymentOrderParam
        restInitPaymentOrderParamMockMvc.perform(get("/api/init-payment-order-params/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInitPaymentOrderParam() throws Exception {
        // Initialize the database
        initPaymentOrderParamService.save(initPaymentOrderParam);

        int databaseSizeBeforeUpdate = initPaymentOrderParamRepository.findAll().size();

        // Update the initPaymentOrderParam
        InitPaymentOrderParam updatedInitPaymentOrderParam = initPaymentOrderParamRepository.findById(initPaymentOrderParam.getId()).get();
        // Disconnect from session so that the updates on updatedInitPaymentOrderParam are not directly saved in db
        em.detach(updatedInitPaymentOrderParam);
        updatedInitPaymentOrderParam
            .paramName(UPDATED_PARAM_NAME)
            .paramValue(UPDATED_PARAM_VALUE);

        restInitPaymentOrderParamMockMvc.perform(put("/api/init-payment-order-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInitPaymentOrderParam)))
            .andExpect(status().isOk());

        // Validate the InitPaymentOrderParam in the database
        List<InitPaymentOrderParam> initPaymentOrderParamList = initPaymentOrderParamRepository.findAll();
        assertThat(initPaymentOrderParamList).hasSize(databaseSizeBeforeUpdate);
        InitPaymentOrderParam testInitPaymentOrderParam = initPaymentOrderParamList.get(initPaymentOrderParamList.size() - 1);
        assertThat(testInitPaymentOrderParam.getParamName()).isEqualTo(UPDATED_PARAM_NAME);
        assertThat(testInitPaymentOrderParam.getParamValue()).isEqualTo(UPDATED_PARAM_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingInitPaymentOrderParam() throws Exception {
        int databaseSizeBeforeUpdate = initPaymentOrderParamRepository.findAll().size();

        // Create the InitPaymentOrderParam

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInitPaymentOrderParamMockMvc.perform(put("/api/init-payment-order-params")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(initPaymentOrderParam)))
            .andExpect(status().isBadRequest());

        // Validate the InitPaymentOrderParam in the database
        List<InitPaymentOrderParam> initPaymentOrderParamList = initPaymentOrderParamRepository.findAll();
        assertThat(initPaymentOrderParamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInitPaymentOrderParam() throws Exception {
        // Initialize the database
        initPaymentOrderParamService.save(initPaymentOrderParam);

        int databaseSizeBeforeDelete = initPaymentOrderParamRepository.findAll().size();

        // Delete the initPaymentOrderParam
        restInitPaymentOrderParamMockMvc.perform(delete("/api/init-payment-order-params/{id}", initPaymentOrderParam.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<InitPaymentOrderParam> initPaymentOrderParamList = initPaymentOrderParamRepository.findAll();
        assertThat(initPaymentOrderParamList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InitPaymentOrderParam.class);
        InitPaymentOrderParam initPaymentOrderParam1 = new InitPaymentOrderParam();
        initPaymentOrderParam1.setId(1L);
        InitPaymentOrderParam initPaymentOrderParam2 = new InitPaymentOrderParam();
        initPaymentOrderParam2.setId(initPaymentOrderParam1.getId());
        assertThat(initPaymentOrderParam1).isEqualTo(initPaymentOrderParam2);
        initPaymentOrderParam2.setId(2L);
        assertThat(initPaymentOrderParam1).isNotEqualTo(initPaymentOrderParam2);
        initPaymentOrderParam1.setId(null);
        assertThat(initPaymentOrderParam1).isNotEqualTo(initPaymentOrderParam2);
    }
}
