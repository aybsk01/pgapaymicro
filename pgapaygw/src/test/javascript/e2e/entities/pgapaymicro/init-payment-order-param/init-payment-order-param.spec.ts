// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {
  InitPaymentOrderParamComponentsPage,
  InitPaymentOrderParamDeleteDialog,
  InitPaymentOrderParamUpdatePage
} from './init-payment-order-param.page-object';

const expect = chai.expect;

describe('InitPaymentOrderParam e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let initPaymentOrderParamComponentsPage: InitPaymentOrderParamComponentsPage;
  let initPaymentOrderParamUpdatePage: InitPaymentOrderParamUpdatePage;
  let initPaymentOrderParamDeleteDialog: InitPaymentOrderParamDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load InitPaymentOrderParams', async () => {
    await navBarPage.goToEntity('init-payment-order-param');
    initPaymentOrderParamComponentsPage = new InitPaymentOrderParamComponentsPage();
    await browser.wait(ec.visibilityOf(initPaymentOrderParamComponentsPage.title), 5000);
    expect(await initPaymentOrderParamComponentsPage.getTitle()).to.eq('pgapaygwApp.pgapaymicroInitPaymentOrderParam.home.title');
  });

  it('should load create InitPaymentOrderParam page', async () => {
    await initPaymentOrderParamComponentsPage.clickOnCreateButton();
    initPaymentOrderParamUpdatePage = new InitPaymentOrderParamUpdatePage();
    expect(await initPaymentOrderParamUpdatePage.getPageTitle()).to.eq(
      'pgapaygwApp.pgapaymicroInitPaymentOrderParam.home.createOrEditLabel'
    );
    await initPaymentOrderParamUpdatePage.cancel();
  });

  it('should create and save InitPaymentOrderParams', async () => {
    const nbButtonsBeforeCreate = await initPaymentOrderParamComponentsPage.countDeleteButtons();

    await initPaymentOrderParamComponentsPage.clickOnCreateButton();
    await promise.all([
      initPaymentOrderParamUpdatePage.setParamNameInput('paramName'),
      initPaymentOrderParamUpdatePage.setParamValueInput('paramValue'),
      initPaymentOrderParamUpdatePage.initPaymentParamSelectLastOption()
    ]);
    expect(await initPaymentOrderParamUpdatePage.getParamNameInput()).to.eq(
      'paramName',
      'Expected ParamName value to be equals to paramName'
    );
    expect(await initPaymentOrderParamUpdatePage.getParamValueInput()).to.eq(
      'paramValue',
      'Expected ParamValue value to be equals to paramValue'
    );
    await initPaymentOrderParamUpdatePage.save();
    expect(await initPaymentOrderParamUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await initPaymentOrderParamComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last InitPaymentOrderParam', async () => {
    const nbButtonsBeforeDelete = await initPaymentOrderParamComponentsPage.countDeleteButtons();
    await initPaymentOrderParamComponentsPage.clickOnLastDeleteButton();

    initPaymentOrderParamDeleteDialog = new InitPaymentOrderParamDeleteDialog();
    expect(await initPaymentOrderParamDeleteDialog.getDialogTitle()).to.eq('pgapaygwApp.pgapaymicroInitPaymentOrderParam.delete.question');
    await initPaymentOrderParamDeleteDialog.clickOnConfirmButton();

    expect(await initPaymentOrderParamComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
