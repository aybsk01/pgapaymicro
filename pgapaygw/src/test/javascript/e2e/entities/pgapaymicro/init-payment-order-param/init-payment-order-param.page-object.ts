import { element, by, ElementFinder } from 'protractor';

export class InitPaymentOrderParamComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-init-payment-order-param div table .btn-danger'));
  title = element.all(by.css('jhi-init-payment-order-param div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class InitPaymentOrderParamUpdatePage {
  pageTitle = element(by.id('jhi-init-payment-order-param-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  paramNameInput = element(by.id('field_paramName'));
  paramValueInput = element(by.id('field_paramValue'));
  initPaymentParamSelect = element(by.id('field_initPaymentParam'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setParamNameInput(paramName) {
    await this.paramNameInput.sendKeys(paramName);
  }

  async getParamNameInput() {
    return await this.paramNameInput.getAttribute('value');
  }

  async setParamValueInput(paramValue) {
    await this.paramValueInput.sendKeys(paramValue);
  }

  async getParamValueInput() {
    return await this.paramValueInput.getAttribute('value');
  }

  async initPaymentParamSelectLastOption(timeout?: number) {
    await this.initPaymentParamSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async initPaymentParamSelectOption(option) {
    await this.initPaymentParamSelect.sendKeys(option);
  }

  getInitPaymentParamSelect(): ElementFinder {
    return this.initPaymentParamSelect;
  }

  async getInitPaymentParamSelectedOption() {
    return await this.initPaymentParamSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class InitPaymentOrderParamDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-initPaymentOrderParam-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-initPaymentOrderParam'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
