import { element, by, ElementFinder } from 'protractor';

export class InitPaymentParamComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-init-payment-param div table .btn-danger'));
  title = element.all(by.css('jhi-init-payment-param div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class InitPaymentParamUpdatePage {
  pageTitle = element(by.id('jhi-init-payment-param-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  merchantIdInput = element(by.id('field_merchantId'));
  orderIdInput = element(by.id('field_orderId'));
  userIdInput = element(by.id('field_userId'));
  accountIdInput = element(by.id('field_accountId'));
  ammountInput = element(by.id('field_ammount'));
  feeInput = element(by.id('field_fee'));
  exponentInput = element(by.id('field_exponent'));
  purchaseShortDescInput = element(by.id('field_purchaseShortDesc'));
  purchaseLongDescInput = element(by.id('field_purchaseLongDesc'));
  paymentSelect = element(by.id('field_payment'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setMerchantIdInput(merchantId) {
    await this.merchantIdInput.sendKeys(merchantId);
  }

  async getMerchantIdInput() {
    return await this.merchantIdInput.getAttribute('value');
  }

  async setOrderIdInput(orderId) {
    await this.orderIdInput.sendKeys(orderId);
  }

  async getOrderIdInput() {
    return await this.orderIdInput.getAttribute('value');
  }

  async setUserIdInput(userId) {
    await this.userIdInput.sendKeys(userId);
  }

  async getUserIdInput() {
    return await this.userIdInput.getAttribute('value');
  }

  async setAccountIdInput(accountId) {
    await this.accountIdInput.sendKeys(accountId);
  }

  async getAccountIdInput() {
    return await this.accountIdInput.getAttribute('value');
  }

  async setAmmountInput(ammount) {
    await this.ammountInput.sendKeys(ammount);
  }

  async getAmmountInput() {
    return await this.ammountInput.getAttribute('value');
  }

  async setFeeInput(fee) {
    await this.feeInput.sendKeys(fee);
  }

  async getFeeInput() {
    return await this.feeInput.getAttribute('value');
  }

  async setExponentInput(exponent) {
    await this.exponentInput.sendKeys(exponent);
  }

  async getExponentInput() {
    return await this.exponentInput.getAttribute('value');
  }

  async setPurchaseShortDescInput(purchaseShortDesc) {
    await this.purchaseShortDescInput.sendKeys(purchaseShortDesc);
  }

  async getPurchaseShortDescInput() {
    return await this.purchaseShortDescInput.getAttribute('value');
  }

  async setPurchaseLongDescInput(purchaseLongDesc) {
    await this.purchaseLongDescInput.sendKeys(purchaseLongDesc);
  }

  async getPurchaseLongDescInput() {
    return await this.purchaseLongDescInput.getAttribute('value');
  }

  async paymentSelectLastOption(timeout?: number) {
    await this.paymentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async paymentSelectOption(option) {
    await this.paymentSelect.sendKeys(option);
  }

  getPaymentSelect(): ElementFinder {
    return this.paymentSelect;
  }

  async getPaymentSelectedOption() {
    return await this.paymentSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class InitPaymentParamDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-initPaymentParam-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-initPaymentParam'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
