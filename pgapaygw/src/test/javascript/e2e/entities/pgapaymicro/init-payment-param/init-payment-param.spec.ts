// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { InitPaymentParamComponentsPage, InitPaymentParamDeleteDialog, InitPaymentParamUpdatePage } from './init-payment-param.page-object';

const expect = chai.expect;

describe('InitPaymentParam e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let initPaymentParamComponentsPage: InitPaymentParamComponentsPage;
  let initPaymentParamUpdatePage: InitPaymentParamUpdatePage;
  let initPaymentParamDeleteDialog: InitPaymentParamDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load InitPaymentParams', async () => {
    await navBarPage.goToEntity('init-payment-param');
    initPaymentParamComponentsPage = new InitPaymentParamComponentsPage();
    await browser.wait(ec.visibilityOf(initPaymentParamComponentsPage.title), 5000);
    expect(await initPaymentParamComponentsPage.getTitle()).to.eq('pgapaygwApp.pgapaymicroInitPaymentParam.home.title');
  });

  it('should load create InitPaymentParam page', async () => {
    await initPaymentParamComponentsPage.clickOnCreateButton();
    initPaymentParamUpdatePage = new InitPaymentParamUpdatePage();
    expect(await initPaymentParamUpdatePage.getPageTitle()).to.eq('pgapaygwApp.pgapaymicroInitPaymentParam.home.createOrEditLabel');
    await initPaymentParamUpdatePage.cancel();
  });

  it('should create and save InitPaymentParams', async () => {
    const nbButtonsBeforeCreate = await initPaymentParamComponentsPage.countDeleteButtons();

    await initPaymentParamComponentsPage.clickOnCreateButton();
    await promise.all([
      initPaymentParamUpdatePage.setMerchantIdInput('merchantId'),
      initPaymentParamUpdatePage.setOrderIdInput('orderId'),
      initPaymentParamUpdatePage.setUserIdInput('userId'),
      initPaymentParamUpdatePage.setAccountIdInput('accountId'),
      initPaymentParamUpdatePage.setAmmountInput('5'),
      initPaymentParamUpdatePage.setFeeInput('5'),
      initPaymentParamUpdatePage.setExponentInput('5'),
      initPaymentParamUpdatePage.setPurchaseShortDescInput('purchaseShortDesc'),
      initPaymentParamUpdatePage.setPurchaseLongDescInput('purchaseLongDesc'),
      initPaymentParamUpdatePage.paymentSelectLastOption()
    ]);
    expect(await initPaymentParamUpdatePage.getMerchantIdInput()).to.eq(
      'merchantId',
      'Expected MerchantId value to be equals to merchantId'
    );
    expect(await initPaymentParamUpdatePage.getOrderIdInput()).to.eq('orderId', 'Expected OrderId value to be equals to orderId');
    expect(await initPaymentParamUpdatePage.getUserIdInput()).to.eq('userId', 'Expected UserId value to be equals to userId');
    expect(await initPaymentParamUpdatePage.getAccountIdInput()).to.eq('accountId', 'Expected AccountId value to be equals to accountId');
    expect(await initPaymentParamUpdatePage.getAmmountInput()).to.eq('5', 'Expected ammount value to be equals to 5');
    expect(await initPaymentParamUpdatePage.getFeeInput()).to.eq('5', 'Expected fee value to be equals to 5');
    expect(await initPaymentParamUpdatePage.getExponentInput()).to.eq('5', 'Expected exponent value to be equals to 5');
    expect(await initPaymentParamUpdatePage.getPurchaseShortDescInput()).to.eq(
      'purchaseShortDesc',
      'Expected PurchaseShortDesc value to be equals to purchaseShortDesc'
    );
    expect(await initPaymentParamUpdatePage.getPurchaseLongDescInput()).to.eq(
      'purchaseLongDesc',
      'Expected PurchaseLongDesc value to be equals to purchaseLongDesc'
    );
    await initPaymentParamUpdatePage.save();
    expect(await initPaymentParamUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await initPaymentParamComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last InitPaymentParam', async () => {
    const nbButtonsBeforeDelete = await initPaymentParamComponentsPage.countDeleteButtons();
    await initPaymentParamComponentsPage.clickOnLastDeleteButton();

    initPaymentParamDeleteDialog = new InitPaymentParamDeleteDialog();
    expect(await initPaymentParamDeleteDialog.getDialogTitle()).to.eq('pgapaygwApp.pgapaymicroInitPaymentParam.delete.question');
    await initPaymentParamDeleteDialog.clickOnConfirmButton();

    expect(await initPaymentParamComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
