import { element, by, ElementFinder } from 'protractor';

export class PaymentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-payment div table .btn-danger'));
  title = element.all(by.css('jhi-payment div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class PaymentUpdatePage {
  pageTitle = element(by.id('jhi-payment-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  statusSelect = element(by.id('field_status'));
  pgaTrxIdInput = element(by.id('field_pgaTrxId'));
  resultCodeInput = element(by.id('field_resultCode'));
  extResultCodeInput = element(by.id('field_extResultCode'));
  rrnInput = element(by.id('field_rrn'));
  transactionDtInput = element(by.id('field_transactionDt'));
  cardHolderInput = element(by.id('field_cardHolder'));
  authCodeInput = element(by.id('field_authCode'));
  panInput = element(by.id('field_pan'));
  fullAuthInput = element(by.id('field_fullAuth'));
  paymentcardSelect = element(by.id('field_paymentcard'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setStatusSelect(status) {
    await this.statusSelect.sendKeys(status);
  }

  async getStatusSelect() {
    return await this.statusSelect.element(by.css('option:checked')).getText();
  }

  async statusSelectLastOption(timeout?: number) {
    await this.statusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setPgaTrxIdInput(pgaTrxId) {
    await this.pgaTrxIdInput.sendKeys(pgaTrxId);
  }

  async getPgaTrxIdInput() {
    return await this.pgaTrxIdInput.getAttribute('value');
  }

  async setResultCodeInput(resultCode) {
    await this.resultCodeInput.sendKeys(resultCode);
  }

  async getResultCodeInput() {
    return await this.resultCodeInput.getAttribute('value');
  }

  async setExtResultCodeInput(extResultCode) {
    await this.extResultCodeInput.sendKeys(extResultCode);
  }

  async getExtResultCodeInput() {
    return await this.extResultCodeInput.getAttribute('value');
  }

  async setRrnInput(rrn) {
    await this.rrnInput.sendKeys(rrn);
  }

  async getRrnInput() {
    return await this.rrnInput.getAttribute('value');
  }

  async setTransactionDtInput(transactionDt) {
    await this.transactionDtInput.sendKeys(transactionDt);
  }

  async getTransactionDtInput() {
    return await this.transactionDtInput.getAttribute('value');
  }

  async setCardHolderInput(cardHolder) {
    await this.cardHolderInput.sendKeys(cardHolder);
  }

  async getCardHolderInput() {
    return await this.cardHolderInput.getAttribute('value');
  }

  async setAuthCodeInput(authCode) {
    await this.authCodeInput.sendKeys(authCode);
  }

  async getAuthCodeInput() {
    return await this.authCodeInput.getAttribute('value');
  }

  async setPanInput(pan) {
    await this.panInput.sendKeys(pan);
  }

  async getPanInput() {
    return await this.panInput.getAttribute('value');
  }

  getFullAuthInput(timeout?: number) {
    return this.fullAuthInput;
  }

  async paymentcardSelectLastOption(timeout?: number) {
    await this.paymentcardSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async paymentcardSelectOption(option) {
    await this.paymentcardSelect.sendKeys(option);
  }

  getPaymentcardSelect(): ElementFinder {
    return this.paymentcardSelect;
  }

  async getPaymentcardSelectedOption() {
    return await this.paymentcardSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class PaymentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-payment-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-payment'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
