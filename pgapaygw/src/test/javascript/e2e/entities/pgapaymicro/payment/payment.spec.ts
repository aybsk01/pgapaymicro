// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { PaymentComponentsPage, PaymentDeleteDialog, PaymentUpdatePage } from './payment.page-object';

const expect = chai.expect;

describe('Payment e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let paymentComponentsPage: PaymentComponentsPage;
  let paymentUpdatePage: PaymentUpdatePage;
  let paymentDeleteDialog: PaymentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Payments', async () => {
    await navBarPage.goToEntity('payment');
    paymentComponentsPage = new PaymentComponentsPage();
    await browser.wait(ec.visibilityOf(paymentComponentsPage.title), 5000);
    expect(await paymentComponentsPage.getTitle()).to.eq('pgapaygwApp.pgapaymicroPayment.home.title');
  });

  it('should load create Payment page', async () => {
    await paymentComponentsPage.clickOnCreateButton();
    paymentUpdatePage = new PaymentUpdatePage();
    expect(await paymentUpdatePage.getPageTitle()).to.eq('pgapaygwApp.pgapaymicroPayment.home.createOrEditLabel');
    await paymentUpdatePage.cancel();
  });

  it('should create and save Payments', async () => {
    const nbButtonsBeforeCreate = await paymentComponentsPage.countDeleteButtons();

    await paymentComponentsPage.clickOnCreateButton();
    await promise.all([
      paymentUpdatePage.statusSelectLastOption(),
      paymentUpdatePage.setPgaTrxIdInput('pgaTrxId'),
      paymentUpdatePage.setResultCodeInput('5'),
      paymentUpdatePage.setExtResultCodeInput('extResultCode'),
      paymentUpdatePage.setRrnInput('rrn'),
      paymentUpdatePage.setTransactionDtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      paymentUpdatePage.setCardHolderInput('cardHolder'),
      paymentUpdatePage.setAuthCodeInput('authCode'),
      paymentUpdatePage.setPanInput('pan'),
      paymentUpdatePage.paymentcardSelectLastOption()
    ]);
    expect(await paymentUpdatePage.getPgaTrxIdInput()).to.eq('pgaTrxId', 'Expected PgaTrxId value to be equals to pgaTrxId');
    expect(await paymentUpdatePage.getResultCodeInput()).to.eq('5', 'Expected resultCode value to be equals to 5');
    expect(await paymentUpdatePage.getExtResultCodeInput()).to.eq(
      'extResultCode',
      'Expected ExtResultCode value to be equals to extResultCode'
    );
    expect(await paymentUpdatePage.getRrnInput()).to.eq('rrn', 'Expected Rrn value to be equals to rrn');
    expect(await paymentUpdatePage.getTransactionDtInput()).to.contain(
      '2001-01-01T02:30',
      'Expected transactionDt value to be equals to 2000-12-31'
    );
    expect(await paymentUpdatePage.getCardHolderInput()).to.eq('cardHolder', 'Expected CardHolder value to be equals to cardHolder');
    expect(await paymentUpdatePage.getAuthCodeInput()).to.eq('authCode', 'Expected AuthCode value to be equals to authCode');
    expect(await paymentUpdatePage.getPanInput()).to.eq('pan', 'Expected Pan value to be equals to pan');
    const selectedFullAuth = paymentUpdatePage.getFullAuthInput();
    if (await selectedFullAuth.isSelected()) {
      await paymentUpdatePage.getFullAuthInput().click();
      expect(await paymentUpdatePage.getFullAuthInput().isSelected(), 'Expected fullAuth not to be selected').to.be.false;
    } else {
      await paymentUpdatePage.getFullAuthInput().click();
      expect(await paymentUpdatePage.getFullAuthInput().isSelected(), 'Expected fullAuth to be selected').to.be.true;
    }
    await paymentUpdatePage.save();
    expect(await paymentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await paymentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Payment', async () => {
    const nbButtonsBeforeDelete = await paymentComponentsPage.countDeleteButtons();
    await paymentComponentsPage.clickOnLastDeleteButton();

    paymentDeleteDialog = new PaymentDeleteDialog();
    expect(await paymentDeleteDialog.getDialogTitle()).to.eq('pgapaygwApp.pgapaymicroPayment.delete.question');
    await paymentDeleteDialog.clickOnConfirmButton();

    expect(await paymentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
