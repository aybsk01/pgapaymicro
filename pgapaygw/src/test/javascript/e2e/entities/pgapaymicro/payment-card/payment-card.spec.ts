// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { PaymentCardComponentsPage, PaymentCardDeleteDialog, PaymentCardUpdatePage } from './payment-card.page-object';

const expect = chai.expect;

describe('PaymentCard e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let paymentCardComponentsPage: PaymentCardComponentsPage;
  let paymentCardUpdatePage: PaymentCardUpdatePage;
  let paymentCardDeleteDialog: PaymentCardDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load PaymentCards', async () => {
    await navBarPage.goToEntity('payment-card');
    paymentCardComponentsPage = new PaymentCardComponentsPage();
    await browser.wait(ec.visibilityOf(paymentCardComponentsPage.title), 5000);
    expect(await paymentCardComponentsPage.getTitle()).to.eq('pgapaygwApp.pgapaymicroPaymentCard.home.title');
  });

  it('should load create PaymentCard page', async () => {
    await paymentCardComponentsPage.clickOnCreateButton();
    paymentCardUpdatePage = new PaymentCardUpdatePage();
    expect(await paymentCardUpdatePage.getPageTitle()).to.eq('pgapaygwApp.pgapaymicroPaymentCard.home.createOrEditLabel');
    await paymentCardUpdatePage.cancel();
  });

  it('should create and save PaymentCards', async () => {
    const nbButtonsBeforeCreate = await paymentCardComponentsPage.countDeleteButtons();

    await paymentCardComponentsPage.clickOnCreateButton();
    await promise.all([
      paymentCardUpdatePage.setUserIdInput('userId'),
      paymentCardUpdatePage.setCardIdInput('cardId'),
      paymentCardUpdatePage.setExpireDtInput('expireDt')
    ]);
    expect(await paymentCardUpdatePage.getUserIdInput()).to.eq('userId', 'Expected UserId value to be equals to userId');
    expect(await paymentCardUpdatePage.getCardIdInput()).to.eq('cardId', 'Expected CardId value to be equals to cardId');
    expect(await paymentCardUpdatePage.getExpireDtInput()).to.eq('expireDt', 'Expected ExpireDt value to be equals to expireDt');
    const selectedRequrrent = paymentCardUpdatePage.getRequrrentInput();
    if (await selectedRequrrent.isSelected()) {
      await paymentCardUpdatePage.getRequrrentInput().click();
      expect(await paymentCardUpdatePage.getRequrrentInput().isSelected(), 'Expected requrrent not to be selected').to.be.false;
    } else {
      await paymentCardUpdatePage.getRequrrentInput().click();
      expect(await paymentCardUpdatePage.getRequrrentInput().isSelected(), 'Expected requrrent to be selected').to.be.true;
    }
    await paymentCardUpdatePage.save();
    expect(await paymentCardUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await paymentCardComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last PaymentCard', async () => {
    const nbButtonsBeforeDelete = await paymentCardComponentsPage.countDeleteButtons();
    await paymentCardComponentsPage.clickOnLastDeleteButton();

    paymentCardDeleteDialog = new PaymentCardDeleteDialog();
    expect(await paymentCardDeleteDialog.getDialogTitle()).to.eq('pgapaygwApp.pgapaymicroPaymentCard.delete.question');
    await paymentCardDeleteDialog.clickOnConfirmButton();

    expect(await paymentCardComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
