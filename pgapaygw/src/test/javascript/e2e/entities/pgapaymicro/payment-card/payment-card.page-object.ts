import { element, by, ElementFinder } from 'protractor';

export class PaymentCardComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-payment-card div table .btn-danger'));
  title = element.all(by.css('jhi-payment-card div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class PaymentCardUpdatePage {
  pageTitle = element(by.id('jhi-payment-card-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  userIdInput = element(by.id('field_userId'));
  cardIdInput = element(by.id('field_cardId'));
  expireDtInput = element(by.id('field_expireDt'));
  requrrentInput = element(by.id('field_requrrent'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setUserIdInput(userId) {
    await this.userIdInput.sendKeys(userId);
  }

  async getUserIdInput() {
    return await this.userIdInput.getAttribute('value');
  }

  async setCardIdInput(cardId) {
    await this.cardIdInput.sendKeys(cardId);
  }

  async getCardIdInput() {
    return await this.cardIdInput.getAttribute('value');
  }

  async setExpireDtInput(expireDt) {
    await this.expireDtInput.sendKeys(expireDt);
  }

  async getExpireDtInput() {
    return await this.expireDtInput.getAttribute('value');
  }

  getRequrrentInput(timeout?: number) {
    return this.requrrentInput;
  }
  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class PaymentCardDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-paymentCard-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-paymentCard'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
