import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PgapaygwTestModule } from '../../../../test.module';
import { InitPaymentParamComponent } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param.component';
import { InitPaymentParamService } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param.service';
import { InitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';

describe('Component Tests', () => {
  describe('InitPaymentParam Management Component', () => {
    let comp: InitPaymentParamComponent;
    let fixture: ComponentFixture<InitPaymentParamComponent>;
    let service: InitPaymentParamService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [InitPaymentParamComponent],
        providers: []
      })
        .overrideTemplate(InitPaymentParamComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InitPaymentParamComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InitPaymentParamService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new InitPaymentParam(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.initPaymentParams[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
