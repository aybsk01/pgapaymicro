import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PgapaygwTestModule } from '../../../../test.module';
import { InitPaymentOrderParamUpdateComponent } from 'app/entities/pgapaymicro/init-payment-order-param/init-payment-order-param-update.component';
import { InitPaymentOrderParamService } from 'app/entities/pgapaymicro/init-payment-order-param/init-payment-order-param.service';
import { InitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';

describe('Component Tests', () => {
  describe('InitPaymentOrderParam Management Update Component', () => {
    let comp: InitPaymentOrderParamUpdateComponent;
    let fixture: ComponentFixture<InitPaymentOrderParamUpdateComponent>;
    let service: InitPaymentOrderParamService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [InitPaymentOrderParamUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(InitPaymentOrderParamUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InitPaymentOrderParamUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InitPaymentOrderParamService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new InitPaymentOrderParam(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new InitPaymentOrderParam();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
