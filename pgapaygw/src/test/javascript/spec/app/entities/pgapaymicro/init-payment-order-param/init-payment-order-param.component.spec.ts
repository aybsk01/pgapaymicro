import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PgapaygwTestModule } from '../../../../test.module';
import { InitPaymentOrderParamComponent } from 'app/entities/pgapaymicro/init-payment-order-param/init-payment-order-param.component';
import { InitPaymentOrderParamService } from 'app/entities/pgapaymicro/init-payment-order-param/init-payment-order-param.service';
import { InitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';

describe('Component Tests', () => {
  describe('InitPaymentOrderParam Management Component', () => {
    let comp: InitPaymentOrderParamComponent;
    let fixture: ComponentFixture<InitPaymentOrderParamComponent>;
    let service: InitPaymentOrderParamService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [InitPaymentOrderParamComponent],
        providers: []
      })
        .overrideTemplate(InitPaymentOrderParamComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InitPaymentOrderParamComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InitPaymentOrderParamService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new InitPaymentOrderParam(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.initPaymentOrderParams[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
