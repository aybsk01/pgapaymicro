import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import { InitPaymentParamService } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param.service';
import { IInitPaymentParam, InitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';

describe('Service Tests', () => {
  describe('InitPaymentParam Service', () => {
    let injector: TestBed;
    let service: InitPaymentParamService;
    let httpMock: HttpTestingController;
    let elemDefault: IInitPaymentParam;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(InitPaymentParamService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new InitPaymentParam(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0, 0, 0, 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a InitPaymentParam', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new InitPaymentParam(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a InitPaymentParam', () => {
        const returnedFromService = Object.assign(
          {
            merchantId: 'BBBBBB',
            orderId: 'BBBBBB',
            userId: 'BBBBBB',
            accountId: 'BBBBBB',
            ammount: 1,
            fee: 1,
            exponent: 1,
            purchaseShortDesc: 'BBBBBB',
            purchaseLongDesc: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of InitPaymentParam', () => {
        const returnedFromService = Object.assign(
          {
            merchantId: 'BBBBBB',
            orderId: 'BBBBBB',
            userId: 'BBBBBB',
            accountId: 'BBBBBB',
            ammount: 1,
            fee: 1,
            exponent: 1,
            purchaseShortDesc: 'BBBBBB',
            purchaseLongDesc: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a InitPaymentParam', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
