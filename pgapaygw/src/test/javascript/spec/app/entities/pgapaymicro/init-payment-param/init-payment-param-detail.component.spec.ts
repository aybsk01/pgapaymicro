import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PgapaygwTestModule } from '../../../../test.module';
import { InitPaymentParamDetailComponent } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param-detail.component';
import { InitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';

describe('Component Tests', () => {
  describe('InitPaymentParam Management Detail Component', () => {
    let comp: InitPaymentParamDetailComponent;
    let fixture: ComponentFixture<InitPaymentParamDetailComponent>;
    const route = ({ data: of({ initPaymentParam: new InitPaymentParam(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [InitPaymentParamDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(InitPaymentParamDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(InitPaymentParamDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.initPaymentParam).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
