import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PgapaygwTestModule } from '../../../../test.module';
import { InitPaymentParamDeleteDialogComponent } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param-delete-dialog.component';
import { InitPaymentParamService } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param.service';

describe('Component Tests', () => {
  describe('InitPaymentParam Management Delete Component', () => {
    let comp: InitPaymentParamDeleteDialogComponent;
    let fixture: ComponentFixture<InitPaymentParamDeleteDialogComponent>;
    let service: InitPaymentParamService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [InitPaymentParamDeleteDialogComponent]
      })
        .overrideTemplate(InitPaymentParamDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(InitPaymentParamDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InitPaymentParamService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
