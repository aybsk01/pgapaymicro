import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PgapaygwTestModule } from '../../../../test.module';
import { PaymentCardDetailComponent } from 'app/entities/pgapaymicro/payment-card/payment-card-detail.component';
import { PaymentCard } from 'app/shared/model/pgapaymicro/payment-card.model';

describe('Component Tests', () => {
  describe('PaymentCard Management Detail Component', () => {
    let comp: PaymentCardDetailComponent;
    let fixture: ComponentFixture<PaymentCardDetailComponent>;
    const route = ({ data: of({ paymentCard: new PaymentCard(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [PaymentCardDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PaymentCardDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PaymentCardDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.paymentCard).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
