import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PgapaygwTestModule } from '../../../../test.module';
import { InitPaymentParamUpdateComponent } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param-update.component';
import { InitPaymentParamService } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param.service';
import { InitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';

describe('Component Tests', () => {
  describe('InitPaymentParam Management Update Component', () => {
    let comp: InitPaymentParamUpdateComponent;
    let fixture: ComponentFixture<InitPaymentParamUpdateComponent>;
    let service: InitPaymentParamService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [InitPaymentParamUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(InitPaymentParamUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InitPaymentParamUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InitPaymentParamService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new InitPaymentParam(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new InitPaymentParam();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
