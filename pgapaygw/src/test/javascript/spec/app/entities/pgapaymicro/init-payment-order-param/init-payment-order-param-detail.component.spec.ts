import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PgapaygwTestModule } from '../../../../test.module';
import { InitPaymentOrderParamDetailComponent } from 'app/entities/pgapaymicro/init-payment-order-param/init-payment-order-param-detail.component';
import { InitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';

describe('Component Tests', () => {
  describe('InitPaymentOrderParam Management Detail Component', () => {
    let comp: InitPaymentOrderParamDetailComponent;
    let fixture: ComponentFixture<InitPaymentOrderParamDetailComponent>;
    const route = ({ data: of({ initPaymentOrderParam: new InitPaymentOrderParam(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [InitPaymentOrderParamDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(InitPaymentOrderParamDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(InitPaymentOrderParamDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.initPaymentOrderParam).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
