import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PgapaygwTestModule } from '../../../../test.module';
import { InitPaymentOrderParamDeleteDialogComponent } from 'app/entities/pgapaymicro/init-payment-order-param/init-payment-order-param-delete-dialog.component';
import { InitPaymentOrderParamService } from 'app/entities/pgapaymicro/init-payment-order-param/init-payment-order-param.service';

describe('Component Tests', () => {
  describe('InitPaymentOrderParam Management Delete Component', () => {
    let comp: InitPaymentOrderParamDeleteDialogComponent;
    let fixture: ComponentFixture<InitPaymentOrderParamDeleteDialogComponent>;
    let service: InitPaymentOrderParamService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PgapaygwTestModule],
        declarations: [InitPaymentOrderParamDeleteDialogComponent]
      })
        .overrideTemplate(InitPaymentOrderParamDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(InitPaymentOrderParamDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InitPaymentOrderParamService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
