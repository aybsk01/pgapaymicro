/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.iconsoft.pgapaygw.web.rest.vm;
