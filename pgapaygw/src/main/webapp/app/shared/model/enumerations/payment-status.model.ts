export const enum PaymentStatus {
  NEW = 'NEW',
  COMPLETE = 'COMPLETE',
  ERROR = 'ERROR'
}
