export interface IPaymentCard {
  id?: number;
  userId?: string;
  cardId?: string;
  expireDt?: string;
  requrrent?: boolean;
}

export class PaymentCard implements IPaymentCard {
  constructor(public id?: number, public userId?: string, public cardId?: string, public expireDt?: string, public requrrent?: boolean) {
    this.requrrent = this.requrrent || false;
  }
}
