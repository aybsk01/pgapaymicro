import { Moment } from 'moment';
import { IPaymentCard } from 'app/shared/model/pgapaymicro/payment-card.model';
import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';
import { PaymentStatus } from 'app/shared/model/enumerations/payment-status.model';

export interface IPayment {
  id?: number;
  status?: PaymentStatus;
  pgaTrxId?: string;
  resultCode?: number;
  extResultCode?: string;
  rrn?: string;
  transactionDt?: Moment;
  cardHolder?: string;
  authCode?: string;
  pan?: string;
  fullAuth?: boolean;
  paymentcard?: IPaymentCard;
  initPaymentParam?: IInitPaymentParam;
}

export class Payment implements IPayment {
  constructor(
    public id?: number,
    public status?: PaymentStatus,
    public pgaTrxId?: string,
    public resultCode?: number,
    public extResultCode?: string,
    public rrn?: string,
    public transactionDt?: Moment,
    public cardHolder?: string,
    public authCode?: string,
    public pan?: string,
    public fullAuth?: boolean,
    public paymentcard?: IPaymentCard,
    public initPaymentParam?: IInitPaymentParam
  ) {
    this.fullAuth = this.fullAuth || false;
  }
}
