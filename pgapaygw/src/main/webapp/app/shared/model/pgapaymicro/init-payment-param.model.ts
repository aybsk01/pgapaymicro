import { IPayment } from 'app/shared/model/pgapaymicro/payment.model';
import { IInitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';

export interface IInitPaymentParam {
  id?: number;
  merchantId?: string;
  orderId?: string;
  userId?: string;
  accountId?: string;
  ammount?: number;
  fee?: number;
  exponent?: number;
  purchaseShortDesc?: string;
  purchaseLongDesc?: string;
  payment?: IPayment;
  initPaymentOrderParams?: IInitPaymentOrderParam[];
}

export class InitPaymentParam implements IInitPaymentParam {
  constructor(
    public id?: number,
    public merchantId?: string,
    public orderId?: string,
    public userId?: string,
    public accountId?: string,
    public ammount?: number,
    public fee?: number,
    public exponent?: number,
    public purchaseShortDesc?: string,
    public purchaseLongDesc?: string,
    public payment?: IPayment,
    public initPaymentOrderParams?: IInitPaymentOrderParam[]
  ) {}
}
