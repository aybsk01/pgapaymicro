import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';

export interface IInitPaymentOrderParam {
  id?: number;
  paramName?: string;
  paramValue?: string;
  initPaymentParam?: IInitPaymentParam;
}

export class InitPaymentOrderParam implements IInitPaymentOrderParam {
  constructor(public id?: number, public paramName?: string, public paramValue?: string, public initPaymentParam?: IInitPaymentParam) {}
}
