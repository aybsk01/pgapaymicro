export interface IMerchant {
  id?: number;
  merchid?: string;
  name?: string;
}

export class Merchant implements IMerchant {
  constructor(public id?: number, public merchid?: string, public name?: string) {}
}
