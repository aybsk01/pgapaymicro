import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'merchant',
        loadChildren: () => import('./merchant/merchant.module').then(m => m.PgapaygwMerchantModule)
      },
      {
        path: 'init-payment-param',
        loadChildren: () =>
          import('./pgapaymicro/init-payment-param/init-payment-param.module').then(m => m.PgapaymicroInitPaymentParamModule)
      },
      {
        path: 'init-payment-order-param',
        loadChildren: () =>
          import('./pgapaymicro/init-payment-order-param/init-payment-order-param.module').then(
            m => m.PgapaymicroInitPaymentOrderParamModule
          )
      },
      {
        path: 'payment',
        loadChildren: () => import('./pgapaymicro/payment/payment.module').then(m => m.PgapaymicroPaymentModule)
      },
      {
        path: 'payment-card',
        loadChildren: () => import('./pgapaymicro/payment-card/payment-card.module').then(m => m.PgapaymicroPaymentCardModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class PgapaygwEntityModule {}
