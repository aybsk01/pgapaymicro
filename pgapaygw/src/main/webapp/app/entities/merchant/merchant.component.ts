import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager } from 'ng-jhipster';

import { IMerchant } from 'app/shared/model/merchant.model';
import { AccountService } from 'app/core/auth/account.service';
import { MerchantService } from './merchant.service';

@Component({
  selector: 'jhi-merchant',
  templateUrl: './merchant.component.html'
})
export class MerchantComponent implements OnInit, OnDestroy {
  merchants: IMerchant[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected merchantService: MerchantService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.merchantService
      .query()
      .pipe(
        filter((res: HttpResponse<IMerchant[]>) => res.ok),
        map((res: HttpResponse<IMerchant[]>) => res.body)
      )
      .subscribe((res: IMerchant[]) => {
        this.merchants = res;
      });
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInMerchants();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IMerchant) {
    return item.id;
  }

  registerChangeInMerchants() {
    this.eventSubscriber = this.eventManager.subscribe('merchantListModification', response => this.loadAll());
  }
}
