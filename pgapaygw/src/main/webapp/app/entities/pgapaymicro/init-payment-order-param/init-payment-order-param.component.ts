import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager } from 'ng-jhipster';

import { IInitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';
import { AccountService } from 'app/core/auth/account.service';
import { InitPaymentOrderParamService } from './init-payment-order-param.service';

@Component({
  selector: 'jhi-init-payment-order-param',
  templateUrl: './init-payment-order-param.component.html'
})
export class InitPaymentOrderParamComponent implements OnInit, OnDestroy {
  initPaymentOrderParams: IInitPaymentOrderParam[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected initPaymentOrderParamService: InitPaymentOrderParamService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.initPaymentOrderParamService
      .query()
      .pipe(
        filter((res: HttpResponse<IInitPaymentOrderParam[]>) => res.ok),
        map((res: HttpResponse<IInitPaymentOrderParam[]>) => res.body)
      )
      .subscribe((res: IInitPaymentOrderParam[]) => {
        this.initPaymentOrderParams = res;
      });
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInInitPaymentOrderParams();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IInitPaymentOrderParam) {
    return item.id;
  }

  registerChangeInInitPaymentOrderParams() {
    this.eventSubscriber = this.eventManager.subscribe('initPaymentOrderParamListModification', response => this.loadAll());
  }
}
