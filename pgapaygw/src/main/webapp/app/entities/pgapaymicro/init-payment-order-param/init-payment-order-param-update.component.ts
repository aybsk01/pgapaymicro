import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IInitPaymentOrderParam, InitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';
import { InitPaymentOrderParamService } from './init-payment-order-param.service';
import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';
import { InitPaymentParamService } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param.service';

@Component({
  selector: 'jhi-init-payment-order-param-update',
  templateUrl: './init-payment-order-param-update.component.html'
})
export class InitPaymentOrderParamUpdateComponent implements OnInit {
  isSaving: boolean;

  initpaymentparams: IInitPaymentParam[];

  editForm = this.fb.group({
    id: [],
    paramName: [null, [Validators.maxLength(32)]],
    paramValue: [null, [Validators.maxLength(128)]],
    initPaymentParam: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected initPaymentOrderParamService: InitPaymentOrderParamService,
    protected initPaymentParamService: InitPaymentParamService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ initPaymentOrderParam }) => {
      this.updateForm(initPaymentOrderParam);
    });
    this.initPaymentParamService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IInitPaymentParam[]>) => mayBeOk.ok),
        map((response: HttpResponse<IInitPaymentParam[]>) => response.body)
      )
      .subscribe((res: IInitPaymentParam[]) => (this.initpaymentparams = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(initPaymentOrderParam: IInitPaymentOrderParam) {
    this.editForm.patchValue({
      id: initPaymentOrderParam.id,
      paramName: initPaymentOrderParam.paramName,
      paramValue: initPaymentOrderParam.paramValue,
      initPaymentParam: initPaymentOrderParam.initPaymentParam
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const initPaymentOrderParam = this.createFromForm();
    if (initPaymentOrderParam.id !== undefined) {
      this.subscribeToSaveResponse(this.initPaymentOrderParamService.update(initPaymentOrderParam));
    } else {
      this.subscribeToSaveResponse(this.initPaymentOrderParamService.create(initPaymentOrderParam));
    }
  }

  private createFromForm(): IInitPaymentOrderParam {
    return {
      ...new InitPaymentOrderParam(),
      id: this.editForm.get(['id']).value,
      paramName: this.editForm.get(['paramName']).value,
      paramValue: this.editForm.get(['paramValue']).value,
      initPaymentParam: this.editForm.get(['initPaymentParam']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInitPaymentOrderParam>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackInitPaymentParamById(index: number, item: IInitPaymentParam) {
    return item.id;
  }
}
