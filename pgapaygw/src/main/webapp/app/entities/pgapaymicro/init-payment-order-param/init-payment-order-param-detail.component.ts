import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';

@Component({
  selector: 'jhi-init-payment-order-param-detail',
  templateUrl: './init-payment-order-param-detail.component.html'
})
export class InitPaymentOrderParamDetailComponent implements OnInit {
  initPaymentOrderParam: IInitPaymentOrderParam;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ initPaymentOrderParam }) => {
      this.initPaymentOrderParam = initPaymentOrderParam;
    });
  }

  previousState() {
    window.history.back();
  }
}
