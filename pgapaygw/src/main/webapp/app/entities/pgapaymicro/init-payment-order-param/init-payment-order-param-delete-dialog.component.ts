import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';
import { InitPaymentOrderParamService } from './init-payment-order-param.service';

@Component({
  selector: 'jhi-init-payment-order-param-delete-dialog',
  templateUrl: './init-payment-order-param-delete-dialog.component.html'
})
export class InitPaymentOrderParamDeleteDialogComponent {
  initPaymentOrderParam: IInitPaymentOrderParam;

  constructor(
    protected initPaymentOrderParamService: InitPaymentOrderParamService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.initPaymentOrderParamService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'initPaymentOrderParamListModification',
        content: 'Deleted an initPaymentOrderParam'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-init-payment-order-param-delete-popup',
  template: ''
})
export class InitPaymentOrderParamDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ initPaymentOrderParam }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(InitPaymentOrderParamDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.initPaymentOrderParam = initPaymentOrderParam;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/init-payment-order-param', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/init-payment-order-param', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
