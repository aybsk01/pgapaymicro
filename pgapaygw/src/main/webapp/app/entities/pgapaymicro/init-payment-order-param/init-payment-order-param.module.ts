import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PgapaygwSharedModule } from 'app/shared/shared.module';
import { InitPaymentOrderParamComponent } from './init-payment-order-param.component';
import { InitPaymentOrderParamDetailComponent } from './init-payment-order-param-detail.component';
import { InitPaymentOrderParamUpdateComponent } from './init-payment-order-param-update.component';
import {
  InitPaymentOrderParamDeletePopupComponent,
  InitPaymentOrderParamDeleteDialogComponent
} from './init-payment-order-param-delete-dialog.component';
import { initPaymentOrderParamRoute, initPaymentOrderParamPopupRoute } from './init-payment-order-param.route';

const ENTITY_STATES = [...initPaymentOrderParamRoute, ...initPaymentOrderParamPopupRoute];

@NgModule({
  imports: [PgapaygwSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    InitPaymentOrderParamComponent,
    InitPaymentOrderParamDetailComponent,
    InitPaymentOrderParamUpdateComponent,
    InitPaymentOrderParamDeleteDialogComponent,
    InitPaymentOrderParamDeletePopupComponent
  ],
  entryComponents: [InitPaymentOrderParamDeleteDialogComponent]
})
export class PgapaymicroInitPaymentOrderParamModule {}
