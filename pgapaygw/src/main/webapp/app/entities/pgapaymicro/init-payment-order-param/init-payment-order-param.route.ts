import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { InitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';
import { InitPaymentOrderParamService } from './init-payment-order-param.service';
import { InitPaymentOrderParamComponent } from './init-payment-order-param.component';
import { InitPaymentOrderParamDetailComponent } from './init-payment-order-param-detail.component';
import { InitPaymentOrderParamUpdateComponent } from './init-payment-order-param-update.component';
import { InitPaymentOrderParamDeletePopupComponent } from './init-payment-order-param-delete-dialog.component';
import { IInitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';

@Injectable({ providedIn: 'root' })
export class InitPaymentOrderParamResolve implements Resolve<IInitPaymentOrderParam> {
  constructor(private service: InitPaymentOrderParamService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IInitPaymentOrderParam> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<InitPaymentOrderParam>) => response.ok),
        map((initPaymentOrderParam: HttpResponse<InitPaymentOrderParam>) => initPaymentOrderParam.body)
      );
    }
    return of(new InitPaymentOrderParam());
  }
}

export const initPaymentOrderParamRoute: Routes = [
  {
    path: '',
    component: InitPaymentOrderParamComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentOrderParam.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: InitPaymentOrderParamDetailComponent,
    resolve: {
      initPaymentOrderParam: InitPaymentOrderParamResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentOrderParam.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: InitPaymentOrderParamUpdateComponent,
    resolve: {
      initPaymentOrderParam: InitPaymentOrderParamResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentOrderParam.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: InitPaymentOrderParamUpdateComponent,
    resolve: {
      initPaymentOrderParam: InitPaymentOrderParamResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentOrderParam.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const initPaymentOrderParamPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: InitPaymentOrderParamDeletePopupComponent,
    resolve: {
      initPaymentOrderParam: InitPaymentOrderParamResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentOrderParam.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
