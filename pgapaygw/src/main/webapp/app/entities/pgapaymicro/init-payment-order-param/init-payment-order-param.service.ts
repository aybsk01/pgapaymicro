import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IInitPaymentOrderParam } from 'app/shared/model/pgapaymicro/init-payment-order-param.model';

type EntityResponseType = HttpResponse<IInitPaymentOrderParam>;
type EntityArrayResponseType = HttpResponse<IInitPaymentOrderParam[]>;

@Injectable({ providedIn: 'root' })
export class InitPaymentOrderParamService {
  public resourceUrl = SERVER_API_URL + 'services/pgapaymicro/api/init-payment-order-params';

  constructor(protected http: HttpClient) {}

  create(initPaymentOrderParam: IInitPaymentOrderParam): Observable<EntityResponseType> {
    return this.http.post<IInitPaymentOrderParam>(this.resourceUrl, initPaymentOrderParam, { observe: 'response' });
  }

  update(initPaymentOrderParam: IInitPaymentOrderParam): Observable<EntityResponseType> {
    return this.http.put<IInitPaymentOrderParam>(this.resourceUrl, initPaymentOrderParam, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IInitPaymentOrderParam>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInitPaymentOrderParam[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
