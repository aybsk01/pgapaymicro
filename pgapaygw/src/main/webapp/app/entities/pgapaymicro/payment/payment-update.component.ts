import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IPayment, Payment } from 'app/shared/model/pgapaymicro/payment.model';
import { PaymentService } from './payment.service';
import { IPaymentCard } from 'app/shared/model/pgapaymicro/payment-card.model';
import { PaymentCardService } from 'app/entities/pgapaymicro/payment-card/payment-card.service';
import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';
import { InitPaymentParamService } from 'app/entities/pgapaymicro/init-payment-param/init-payment-param.service';

@Component({
  selector: 'jhi-payment-update',
  templateUrl: './payment-update.component.html'
})
export class PaymentUpdateComponent implements OnInit {
  isSaving: boolean;

  paymentcards: IPaymentCard[];

  initpaymentparams: IInitPaymentParam[];

  editForm = this.fb.group({
    id: [],
    status: [null, [Validators.required]],
    pgaTrxId: [null, [Validators.maxLength(32)]],
    resultCode: [],
    extResultCode: [null, [Validators.maxLength(32)]],
    rrn: [],
    transactionDt: [],
    cardHolder: [null, [Validators.maxLength(26)]],
    authCode: [null, [Validators.maxLength(20)]],
    pan: [null, [Validators.maxLength(19)]],
    fullAuth: [],
    paymentcard: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected paymentService: PaymentService,
    protected paymentCardService: PaymentCardService,
    protected initPaymentParamService: InitPaymentParamService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ payment }) => {
      this.updateForm(payment);
    });
    this.paymentCardService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPaymentCard[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPaymentCard[]>) => response.body)
      )
      .subscribe((res: IPaymentCard[]) => (this.paymentcards = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.initPaymentParamService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IInitPaymentParam[]>) => mayBeOk.ok),
        map((response: HttpResponse<IInitPaymentParam[]>) => response.body)
      )
      .subscribe((res: IInitPaymentParam[]) => (this.initpaymentparams = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(payment: IPayment) {
    this.editForm.patchValue({
      id: payment.id,
      status: payment.status,
      pgaTrxId: payment.pgaTrxId,
      resultCode: payment.resultCode,
      extResultCode: payment.extResultCode,
      rrn: payment.rrn,
      transactionDt: payment.transactionDt != null ? payment.transactionDt.format(DATE_TIME_FORMAT) : null,
      cardHolder: payment.cardHolder,
      authCode: payment.authCode,
      pan: payment.pan,
      fullAuth: payment.fullAuth,
      paymentcard: payment.paymentcard
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const payment = this.createFromForm();
    if (payment.id !== undefined) {
      this.subscribeToSaveResponse(this.paymentService.update(payment));
    } else {
      this.subscribeToSaveResponse(this.paymentService.create(payment));
    }
  }

  private createFromForm(): IPayment {
    return {
      ...new Payment(),
      id: this.editForm.get(['id']).value,
      status: this.editForm.get(['status']).value,
      pgaTrxId: this.editForm.get(['pgaTrxId']).value,
      resultCode: this.editForm.get(['resultCode']).value,
      extResultCode: this.editForm.get(['extResultCode']).value,
      rrn: this.editForm.get(['rrn']).value,
      transactionDt:
        this.editForm.get(['transactionDt']).value != null
          ? moment(this.editForm.get(['transactionDt']).value, DATE_TIME_FORMAT)
          : undefined,
      cardHolder: this.editForm.get(['cardHolder']).value,
      authCode: this.editForm.get(['authCode']).value,
      pan: this.editForm.get(['pan']).value,
      fullAuth: this.editForm.get(['fullAuth']).value,
      paymentcard: this.editForm.get(['paymentcard']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPayment>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackPaymentCardById(index: number, item: IPaymentCard) {
    return item.id;
  }

  trackInitPaymentParamById(index: number, item: IInitPaymentParam) {
    return item.id;
  }
}
