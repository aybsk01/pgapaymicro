import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';

type EntityResponseType = HttpResponse<IInitPaymentParam>;
type EntityArrayResponseType = HttpResponse<IInitPaymentParam[]>;

@Injectable({ providedIn: 'root' })
export class InitPaymentParamService {
  public resourceUrl = SERVER_API_URL + 'services/pgapaymicro/api/init-payment-params';

  constructor(protected http: HttpClient) {}

  create(initPaymentParam: IInitPaymentParam): Observable<EntityResponseType> {
    return this.http.post<IInitPaymentParam>(this.resourceUrl, initPaymentParam, { observe: 'response' });
  }

  update(initPaymentParam: IInitPaymentParam): Observable<EntityResponseType> {
    return this.http.put<IInitPaymentParam>(this.resourceUrl, initPaymentParam, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IInitPaymentParam>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInitPaymentParam[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
