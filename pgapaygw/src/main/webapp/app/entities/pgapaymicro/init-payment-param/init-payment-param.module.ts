import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PgapaygwSharedModule } from 'app/shared/shared.module';
import { InitPaymentParamComponent } from './init-payment-param.component';
import { InitPaymentParamDetailComponent } from './init-payment-param-detail.component';
import { InitPaymentParamUpdateComponent } from './init-payment-param-update.component';
import { InitPaymentParamDeletePopupComponent, InitPaymentParamDeleteDialogComponent } from './init-payment-param-delete-dialog.component';
import { initPaymentParamRoute, initPaymentParamPopupRoute } from './init-payment-param.route';

const ENTITY_STATES = [...initPaymentParamRoute, ...initPaymentParamPopupRoute];

@NgModule({
  imports: [PgapaygwSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    InitPaymentParamComponent,
    InitPaymentParamDetailComponent,
    InitPaymentParamUpdateComponent,
    InitPaymentParamDeleteDialogComponent,
    InitPaymentParamDeletePopupComponent
  ],
  entryComponents: [InitPaymentParamDeleteDialogComponent]
})
export class PgapaymicroInitPaymentParamModule {}
