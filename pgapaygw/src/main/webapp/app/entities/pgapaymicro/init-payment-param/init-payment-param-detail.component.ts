import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';

@Component({
  selector: 'jhi-init-payment-param-detail',
  templateUrl: './init-payment-param-detail.component.html'
})
export class InitPaymentParamDetailComponent implements OnInit {
  initPaymentParam: IInitPaymentParam;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ initPaymentParam }) => {
      this.initPaymentParam = initPaymentParam;
    });
  }

  previousState() {
    window.history.back();
  }
}
