import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager } from 'ng-jhipster';

import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';
import { AccountService } from 'app/core/auth/account.service';
import { InitPaymentParamService } from './init-payment-param.service';

@Component({
  selector: 'jhi-init-payment-param',
  templateUrl: './init-payment-param.component.html'
})
export class InitPaymentParamComponent implements OnInit, OnDestroy {
  initPaymentParams: IInitPaymentParam[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected initPaymentParamService: InitPaymentParamService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.initPaymentParamService
      .query()
      .pipe(
        filter((res: HttpResponse<IInitPaymentParam[]>) => res.ok),
        map((res: HttpResponse<IInitPaymentParam[]>) => res.body)
      )
      .subscribe((res: IInitPaymentParam[]) => {
        this.initPaymentParams = res;
      });
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInInitPaymentParams();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IInitPaymentParam) {
    return item.id;
  }

  registerChangeInInitPaymentParams() {
    this.eventSubscriber = this.eventManager.subscribe('initPaymentParamListModification', response => this.loadAll());
  }
}
