import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';
import { InitPaymentParamService } from './init-payment-param.service';

@Component({
  selector: 'jhi-init-payment-param-delete-dialog',
  templateUrl: './init-payment-param-delete-dialog.component.html'
})
export class InitPaymentParamDeleteDialogComponent {
  initPaymentParam: IInitPaymentParam;

  constructor(
    protected initPaymentParamService: InitPaymentParamService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.initPaymentParamService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'initPaymentParamListModification',
        content: 'Deleted an initPaymentParam'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-init-payment-param-delete-popup',
  template: ''
})
export class InitPaymentParamDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ initPaymentParam }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(InitPaymentParamDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.initPaymentParam = initPaymentParam;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/init-payment-param', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/init-payment-param', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
