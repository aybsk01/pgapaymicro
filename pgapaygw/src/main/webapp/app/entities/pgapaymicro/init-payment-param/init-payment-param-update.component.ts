import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IInitPaymentParam, InitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';
import { InitPaymentParamService } from './init-payment-param.service';
import { IPayment } from 'app/shared/model/pgapaymicro/payment.model';
import { PaymentService } from 'app/entities/pgapaymicro/payment/payment.service';

@Component({
  selector: 'jhi-init-payment-param-update',
  templateUrl: './init-payment-param-update.component.html'
})
export class InitPaymentParamUpdateComponent implements OnInit {
  isSaving: boolean;

  payments: IPayment[];

  editForm = this.fb.group({
    id: [],
    merchantId: [null, [Validators.required, Validators.maxLength(32)]],
    orderId: [null, [Validators.required, Validators.maxLength(128)]],
    userId: [],
    accountId: [null, [Validators.maxLength(125)]],
    ammount: [],
    fee: [],
    exponent: [],
    purchaseShortDesc: [null, [Validators.maxLength(30)]],
    purchaseLongDesc: [null, [Validators.maxLength(125)]],
    payment: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected initPaymentParamService: InitPaymentParamService,
    protected paymentService: PaymentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ initPaymentParam }) => {
      this.updateForm(initPaymentParam);
    });
    this.paymentService
      .query({ filter: 'initpaymentparam-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IPayment[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPayment[]>) => response.body)
      )
      .subscribe(
        (res: IPayment[]) => {
          if (!this.editForm.get('payment').value || !this.editForm.get('payment').value.id) {
            this.payments = res;
          } else {
            this.paymentService
              .find(this.editForm.get('payment').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IPayment>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IPayment>) => subResponse.body)
              )
              .subscribe(
                (subRes: IPayment) => (this.payments = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(initPaymentParam: IInitPaymentParam) {
    this.editForm.patchValue({
      id: initPaymentParam.id,
      merchantId: initPaymentParam.merchantId,
      orderId: initPaymentParam.orderId,
      userId: initPaymentParam.userId,
      accountId: initPaymentParam.accountId,
      ammount: initPaymentParam.ammount,
      fee: initPaymentParam.fee,
      exponent: initPaymentParam.exponent,
      purchaseShortDesc: initPaymentParam.purchaseShortDesc,
      purchaseLongDesc: initPaymentParam.purchaseLongDesc,
      payment: initPaymentParam.payment
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const initPaymentParam = this.createFromForm();
    if (initPaymentParam.id !== undefined) {
      this.subscribeToSaveResponse(this.initPaymentParamService.update(initPaymentParam));
    } else {
      this.subscribeToSaveResponse(this.initPaymentParamService.create(initPaymentParam));
    }
  }

  private createFromForm(): IInitPaymentParam {
    return {
      ...new InitPaymentParam(),
      id: this.editForm.get(['id']).value,
      merchantId: this.editForm.get(['merchantId']).value,
      orderId: this.editForm.get(['orderId']).value,
      userId: this.editForm.get(['userId']).value,
      accountId: this.editForm.get(['accountId']).value,
      ammount: this.editForm.get(['ammount']).value,
      fee: this.editForm.get(['fee']).value,
      exponent: this.editForm.get(['exponent']).value,
      purchaseShortDesc: this.editForm.get(['purchaseShortDesc']).value,
      purchaseLongDesc: this.editForm.get(['purchaseLongDesc']).value,
      payment: this.editForm.get(['payment']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInitPaymentParam>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackPaymentById(index: number, item: IPayment) {
    return item.id;
  }
}
