import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { InitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';
import { InitPaymentParamService } from './init-payment-param.service';
import { InitPaymentParamComponent } from './init-payment-param.component';
import { InitPaymentParamDetailComponent } from './init-payment-param-detail.component';
import { InitPaymentParamUpdateComponent } from './init-payment-param-update.component';
import { InitPaymentParamDeletePopupComponent } from './init-payment-param-delete-dialog.component';
import { IInitPaymentParam } from 'app/shared/model/pgapaymicro/init-payment-param.model';

@Injectable({ providedIn: 'root' })
export class InitPaymentParamResolve implements Resolve<IInitPaymentParam> {
  constructor(private service: InitPaymentParamService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IInitPaymentParam> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<InitPaymentParam>) => response.ok),
        map((initPaymentParam: HttpResponse<InitPaymentParam>) => initPaymentParam.body)
      );
    }
    return of(new InitPaymentParam());
  }
}

export const initPaymentParamRoute: Routes = [
  {
    path: '',
    component: InitPaymentParamComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentParam.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: InitPaymentParamDetailComponent,
    resolve: {
      initPaymentParam: InitPaymentParamResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentParam.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: InitPaymentParamUpdateComponent,
    resolve: {
      initPaymentParam: InitPaymentParamResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentParam.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: InitPaymentParamUpdateComponent,
    resolve: {
      initPaymentParam: InitPaymentParamResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentParam.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const initPaymentParamPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: InitPaymentParamDeletePopupComponent,
    resolve: {
      initPaymentParam: InitPaymentParamResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pgapaygwApp.pgapaymicroInitPaymentParam.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
