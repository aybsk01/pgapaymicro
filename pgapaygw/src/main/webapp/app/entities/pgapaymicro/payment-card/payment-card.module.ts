import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PgapaygwSharedModule } from 'app/shared/shared.module';
import { PaymentCardComponent } from './payment-card.component';
import { PaymentCardDetailComponent } from './payment-card-detail.component';
import { PaymentCardUpdateComponent } from './payment-card-update.component';
import { PaymentCardDeletePopupComponent, PaymentCardDeleteDialogComponent } from './payment-card-delete-dialog.component';
import { paymentCardRoute, paymentCardPopupRoute } from './payment-card.route';

const ENTITY_STATES = [...paymentCardRoute, ...paymentCardPopupRoute];

@NgModule({
  imports: [PgapaygwSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PaymentCardComponent,
    PaymentCardDetailComponent,
    PaymentCardUpdateComponent,
    PaymentCardDeleteDialogComponent,
    PaymentCardDeletePopupComponent
  ],
  entryComponents: [PaymentCardDeleteDialogComponent]
})
export class PgapaymicroPaymentCardModule {}
