import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IPaymentCard, PaymentCard } from 'app/shared/model/pgapaymicro/payment-card.model';
import { PaymentCardService } from './payment-card.service';

@Component({
  selector: 'jhi-payment-card-update',
  templateUrl: './payment-card-update.component.html'
})
export class PaymentCardUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    userId: [],
    cardId: [null, [Validators.maxLength(32)]],
    expireDt: [null, [Validators.maxLength(4)]],
    requrrent: []
  });

  constructor(protected paymentCardService: PaymentCardService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ paymentCard }) => {
      this.updateForm(paymentCard);
    });
  }

  updateForm(paymentCard: IPaymentCard) {
    this.editForm.patchValue({
      id: paymentCard.id,
      userId: paymentCard.userId,
      cardId: paymentCard.cardId,
      expireDt: paymentCard.expireDt,
      requrrent: paymentCard.requrrent
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const paymentCard = this.createFromForm();
    if (paymentCard.id !== undefined) {
      this.subscribeToSaveResponse(this.paymentCardService.update(paymentCard));
    } else {
      this.subscribeToSaveResponse(this.paymentCardService.create(paymentCard));
    }
  }

  private createFromForm(): IPaymentCard {
    return {
      ...new PaymentCard(),
      id: this.editForm.get(['id']).value,
      userId: this.editForm.get(['userId']).value,
      cardId: this.editForm.get(['cardId']).value,
      expireDt: this.editForm.get(['expireDt']).value,
      requrrent: this.editForm.get(['requrrent']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPaymentCard>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
