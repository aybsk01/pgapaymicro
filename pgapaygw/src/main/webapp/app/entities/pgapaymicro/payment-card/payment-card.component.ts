import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager } from 'ng-jhipster';

import { IPaymentCard } from 'app/shared/model/pgapaymicro/payment-card.model';
import { AccountService } from 'app/core/auth/account.service';
import { PaymentCardService } from './payment-card.service';

@Component({
  selector: 'jhi-payment-card',
  templateUrl: './payment-card.component.html'
})
export class PaymentCardComponent implements OnInit, OnDestroy {
  paymentCards: IPaymentCard[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected paymentCardService: PaymentCardService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.paymentCardService
      .query()
      .pipe(
        filter((res: HttpResponse<IPaymentCard[]>) => res.ok),
        map((res: HttpResponse<IPaymentCard[]>) => res.body)
      )
      .subscribe((res: IPaymentCard[]) => {
        this.paymentCards = res;
      });
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInPaymentCards();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IPaymentCard) {
    return item.id;
  }

  registerChangeInPaymentCards() {
    this.eventSubscriber = this.eventManager.subscribe('paymentCardListModification', response => this.loadAll());
  }
}
